/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  test-9p.h
 * @brief 9P implementation tests: protocol test helpers
 */

#include <bsd/sys/cdefs.h>
#include <bsd/sys/param.h>

#include <stdio.h>
#include <stdlib.h>

#include <criterion/criterion.h>
#include <criterion/new/assert.h>

#include "pixie.h"
#include "pixie-raw.h"
#include "pixie-raw-prim.h"

////////////////////////////////////////////////////////////////////////

#define MEM(x,n) ((struct cr_mem){ .data = (x), .size = (n) })
#define S(n,x) \
	(pixS){ .s = (n), .text = __DECONST(pix1*, (x)) }


#define MAKE_DE_TEST(k, n, v, fini) \
	Test(raw_9p_##k, de_##n) \
	{ \
		struct pix_raw out; \
		cr_assert (eq (sz, \
			pix_raw_decode ( \
				(bytes_##n), \
				(n_bytes_##n), \
				(v), \
				&out \
			), \
			(n_bytes_##n) \
		)); \
		cr_assert (pix_raw_eq (&out, &(msg_##n))); \
		fini \
	}

#define MAKE_SER_TEST(k, n) \
	Test(raw_9p_##k, ser_##n) \
	{ \
		uint8_t *out; \
		size_t   outlen; \
		cr_assert (eq (sz, \
			pix_raw_encode ( \
				&(msg_##n), \
				&out, \
				&outlen \
			), \
			(n_bytes_##n) \
		)); \
		cr_assert (eq (mem, \
			MEM(out, outlen), \
			MEM((bytes_##n), (n_bytes_##n)) \
		)); \
		free (out); \
	}

////////////////////////////////////////////////////////////////////////
