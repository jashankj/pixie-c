/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  test-9p-r-error.c
 * @brief 9P implementation tests: test Rerror encode/decode
 */

#include "test-9p.h"

////////////////////////////////////////////////////////////////////////

static const size_t n_bytes_1 = 33;
static const uint8_t bytes_1[] = {
	0x21, 0x00, 0x00, 0x00,
	0x6b,
	0x00, 0x00,
	0x18, 0x00,
	0x46, 0x75, 0x6e, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x20, 0x6e, 0x6f, 0x74, 0x20, 0x69, 0x6d, 0x70,
	0x6c, 0x65, 0x6d, 0x65, 0x6e, 0x74, 0x65, 0x64
};

static const struct pix_raw msg_1 = {
	.size = 33,
	.type = R_ERROR,
	.tag  = 0,
	.r_error = { .ename = S(24, "Function not implemented") }
};


MAKE_DE_TEST(r_error, 1, PIX_9P2000, \
({ \
	free (out.r_error.ename.text); \
}); )

MAKE_SER_TEST(r_error, 1)

////////////////////////////////////////////////////////////////////////
