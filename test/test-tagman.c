/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  test-tagman.c
 * @brief 9P implementation tests: tag manager
 */

#include <stdio.h>
#include <stdlib.h>

#include <criterion/criterion.h>

#include "pixie.h"
#include "pixie-tagman.h"

Test(tagman, new_drop)
{
	struct pix_tagman *tagman = pix_tagman_new ();
	cr_assert_not_null (tagman);
	pix_tagman_drop (tagman);
}

Test(tagman, size_alloc_free)
{
	struct pix_tagman *tagman = pix_tagman_new ();
	cr_assert_not_null (tagman,
		"pix_tagman_new() => %p\n", (void *) tagman);

	cr_assert_eq (pix_tagman_size (tagman), 0,
		"pix_tagman_size(#<tagman:%p>) => 0", (void *)tagman);

	pixtag tags[10];
	for (int i = 0; i < 10; i++) {
		tags[i] = pix_tagman_alloc (tagman);
		cr_assert_neq (tags[i], NOTAG,
			"pix_tagman_alloc(#<tagman:%p>) => %u\n", (void *) tagman, tags[i]);
		cr_assert_eq (pix_tagman_size (tagman), i+1,
			"pix_tagman_size(#<tagman:%p>) => %d", (void *)tagman, i+1);
	}

	cr_assert_eq (pix_tagman_size (tagman), 10,
		"pix_tagman_size(#<tagman:%p>) => 10", (void *)tagman);

	for (int i = 9; i >= 0; i--) {
		pix_tagman_free (tagman, tags[i]);
		cr_assert_eq (pix_tagman_size (tagman), i,
			"pix_tagman_free(#<tagman:%p>, %u)\n", (void *) tagman, tags[i]);
	}

	cr_assert_eq (pix_tagman_size (tagman), 0,
		"pix_tagman_size(#<tagman:%p>) => 0", (void *)tagman);

	pix_tagman_drop (tagman);
//	printf ("# pix_tagman_drop(#<tagman:%p>)\n", (void *) tagman);
}
