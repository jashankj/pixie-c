/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  test-raw-prim.c
 * @brief 9P implementation tests: protocol primitives
 */

#include "test-9p.h"

////////////////////////////////////////////////////////////////////////

Test(pix2, de_no_out)
{
	const uint8_t buf[] = { 0x00, 0x00 };
	cr_assert_eq (pix2_de (buf, 0, PIX_9P2000, NULL), -2);
	cr_assert_eq (pix2_de (buf, 1, PIX_9P2000, NULL), -1);
	cr_assert_eq (pix2_de (buf, 2, PIX_9P2000, NULL),  2);
	cr_assert_eq (pix2_de (buf, 3, PIX_9P2000, NULL),  2);
}

Test(pix2, de_1)
{
	const uint8_t buf[] = { 0x06, 0x00 };
	const pix2 exp   = 0x0006;
	pix2 out;

	cr_assert_eq (pix2_de (buf, nitems(buf), PIX_9P2000, &out), 2);
	cr_assert_eq (out, exp);
}

Test(pix2, de_2)
{
	const uint8_t buf[] = { 0x00, 0x06 };
	const pix2 exp   = 0x0600;
	pix2 out;

	cr_assert_eq (pix2_de (buf, nitems(buf), PIX_9P2000, &out), 2);
	cr_assert_eq (out, exp);
}

////////////////////////////////////////////////////////////////////////
