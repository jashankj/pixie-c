/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  test-9p-t-attach.c
 * @brief 9P implementation tests: test Tattach encode/decode
 */

#include "test-9p.h"

////////////////////////////////////////////////////////////////////////

static const size_t n_bytes_1 = 26;
static const uint8_t bytes_1[] = {
	0x1a, 0x00, 0x00, 0x00,
	0x68,
	0x00, 0x00,
	0x00, 0x00, 0x00, 0x00,
	0xff, 0xff, 0xff, 0xff,
	0x07, 0x00,
	0x6a, 0x61, 0x73, 0x68, 0x61, 0x6e, 0x6b,
	0x00, 0x00
};

static const struct pix_raw msg_1 = {
	.size = 26,
	.type = T_ATTACH,
	.tag  = 0,
	.t_attach = {
		.fid  = 0,
		.afid = NOFID,
		.uname = S(7, "jashank"),
		.aname = S(0, ""),
	}
};


MAKE_DE_TEST(t_attach, 1, PIX_9P2000, \
({ \
	free (out.t_attach.uname.text); \
	free (out.t_attach.aname.text); \
}); )

MAKE_SER_TEST(t_attach, 1)

////////////////////////////////////////////////////////////////////////
