/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  test-9p-t-version.c
 * @brief 9P implementation tests: test Tversion encode/decode
 */

#include "test-9p.h"

////////////////////////////////////////////////////////////////////////

static const size_t n_bytes_1 = 19;
static const uint8_t bytes_1[] = {
	0x13, 0x00, 0x00, 0x00,
	0x65,
	0xff, 0xff,
	0x00, 0x20, 0x00, 0x00,
	0x06, 0x00,
	0x39, 0x50, 0x32, 0x30, 0x30, 0x30
};

static const struct pix_raw msg_1 = {
	.size = 19,
	.type = R_VERSION,
	.tag  = NOTAG,
	.r_version = {
		.msize = 8192,
		.version = S(6, "9P2000")
	}
};


MAKE_DE_TEST(r_version, 1, PIX_9P2000, \
({ \
	free (out.r_version.version.text); \
}); )

MAKE_SER_TEST(r_version, 1)

////////////////////////////////////////////////////////////////////////
