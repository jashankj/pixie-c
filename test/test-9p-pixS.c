/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  test-raw-prim.c
 * @brief 9P implementation tests: protocol primitives
 */

#include "test-9p.h"

////////////////////////////////////////////////////////////////////////

Test(pixS, de_no_out)
{
	const uint8_t buf[] = {
		0x06, 0x00,
		0x39, 0x50, 0x32, 0x30, 0x30, 0x30
	};

	cr_assert_eq (pixS_de (buf, 0, PIX_9P2000, NULL), -2);
	cr_assert_eq (pixS_de (buf, 1, PIX_9P2000, NULL), -1);
	cr_assert_eq (pixS_de (buf, 2, PIX_9P2000, NULL), -6);
	cr_assert_eq (pixS_de (buf, 3, PIX_9P2000, NULL), -5);
	cr_assert_eq (pixS_de (buf, 4, PIX_9P2000, NULL), -4);
	cr_assert_eq (pixS_de (buf, 5, PIX_9P2000, NULL), -3);
	cr_assert_eq (pixS_de (buf, 6, PIX_9P2000, NULL), -2);
	cr_assert_eq (pixS_de (buf, 7, PIX_9P2000, NULL), -1);
	cr_assert_eq (pixS_de (buf, 8, PIX_9P2000, NULL),  8);
	cr_assert_eq (pixS_de (buf, 9, PIX_9P2000, NULL),  8);
}

Test(pixS, de_1)
{
	const uint8_t buf[] = {
		0x06, 0x00,
		0x39, 0x50, 0x32, 0x30, 0x30, 0x30
	};

	const pix1 exp_text[] = {0x39, 0x50, 0x32, 0x30, 0x30, 0x30};
	const pixS expect     = { .s = 6, .text = __DECONST(pix1*, exp_text) };

	pixS out;

	cr_assert_eq (pixS_de (buf, nitems (buf), PIX_9P2000, &out), nitems(buf));
	cr_assert_eq (out.s, expect.s);
	cr_assert_arr_eq (out.text, expect.text, expect.s);

	cr_assert_eq (pixS_cmp (&expect, &out), 0);

	free (out.text);
}

////////////////////////////////////////////////////////////////////////
