#-
# Copyright 2021-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

project (pixie
	VERSION		0.1.0
	DESCRIPTION	"yet another 9P2000 protocol implementation"
	HOMEPAGE_URL	"https://gitlab.com/jashankj/pixie-c"
	LANGUAGES	C)
# Keep in step with `CMakeLists.txt'.

add_library (pixie STATIC)

target_sources (pixie PRIVATE
	${pixie_root}/lib/pixie.h
	${pixie_root}/lib/pixie.c
)

target_sources (pixie PRIVATE
	# pix_client
	${pixie_root}/lib/pixie-client.h
	${pixie_root}/lib/pixie-client.c
)

target_sources (pixie PRIVATE
	# pix_xpt
	${pixie_root}/lib/pixie-xpt.h
	${pixie_root}/lib/pixie-xpt-dialparse.c
)

target_sources (pixie PRIVATE
	# pix_tagman
	${pixie_root}/lib/pixie-tagman.h
	${pixie_root}/lib/pixie-tagman.c
)

target_sources (pixie PRIVATE
	# pix_proto
	${pixie_root}/lib/pixie-proto.h
	${pixie_root}/lib/pixie-proto.c
)

target_sources (pixie PRIVATE
	# pix_raw
	${pixie_root}/lib/pixie-raw.h
	${pixie_root}/lib/pixie-raw-msg.h
	${pixie_root}/lib/pixie-raw-prim.h
	${pixie_root}/lib/pixie-cb.h
	${pixie_root}/lib/pixie-raw.c
	${pixie_root}/lib/pixie-9p-pix1.c
	${pixie_root}/lib/pixie-9p-pix2.c
	${pixie_root}/lib/pixie-9p-pix4.c
	${pixie_root}/lib/pixie-9p-pix8.c
	${pixie_root}/lib/pixie-9p-pixS.c
	${pixie_root}/lib/pixie-9p-pixtag.c
	${pixie_root}/lib/pixie-9p-pixtype.c
	${pixie_root}/lib/pixie-9p-pixfid.c
	${pixie_root}/lib/pixie-9p-pixdent.c
	${pixie_root}/lib/pixie-9p-pixqid.c
	${pixie_root}/lib/pixie-9p-t-version.c
	${pixie_root}/lib/pixie-9p-r-version.c
	${pixie_root}/lib/pixie-9p-t-auth.c
	${pixie_root}/lib/pixie-9p-r-auth.c
	${pixie_root}/lib/pixie-9p-t-attach.c
	${pixie_root}/lib/pixie-9p-r-attach.c
	${pixie_root}/lib/pixie-9p-r-error.c
	${pixie_root}/lib/pixie-9p-t-flush.c
	${pixie_root}/lib/pixie-9p-r-flush.c
	${pixie_root}/lib/pixie-9p-t-walk.c
	${pixie_root}/lib/pixie-9p-r-walk.c
	${pixie_root}/lib/pixie-9p-t-open.c
	${pixie_root}/lib/pixie-9p-r-open.c
	${pixie_root}/lib/pixie-9p-t-create.c
	${pixie_root}/lib/pixie-9p-r-create.c
	${pixie_root}/lib/pixie-9p-t-read.c
	${pixie_root}/lib/pixie-9p-r-read.c
	${pixie_root}/lib/pixie-9p-t-write.c
	${pixie_root}/lib/pixie-9p-r-write.c
	${pixie_root}/lib/pixie-9p-t-clunk.c
	${pixie_root}/lib/pixie-9p-r-clunk.c
	${pixie_root}/lib/pixie-9p-t-remove.c
	${pixie_root}/lib/pixie-9p-r-remove.c
	${pixie_root}/lib/pixie-9p-t-stat.c
	${pixie_root}/lib/pixie-9p-r-stat.c
	${pixie_root}/lib/pixie-9p-t-wstat.c
	${pixie_root}/lib/pixie-9p-r-wstat.c
)

target_sources (pixie PRIVATE
	# buf
	${pixie_root}/lib/buf.h
	${pixie_root}/lib/buf.c
)

set_property (TARGET pixie PROPERTY C_STANDARD   23)
set_property (TARGET pixie PROPERTY C_EXTENSIONS ON)
target_compile_options (pixie PRIVATE
	-Wno-c++98-compat
	-Wno-gnu-empty-initializer
	-Wno-empty-translation-unit
	-Wno-implicit-int-conversion
	-Wno-sign-conversion
)
target_compile_options (pixie PUBLIC
	-Wno-macro-redefined
	-Wno-nullability-completeness
)
target_compile_definitions (pixie PRIVATE
	-D_ISOC11_SOURCE
)

target_include_directories (pixie PUBLIC
	${pixie_root}
)

set (pixie_LIBRARIES	pixie)
set (pixie_INCLUDE_DIRS	${pixie_root}/lib)


include (FindPkgConfig)

if (BUILD_TESTS)
	pkg_check_modules (criterion REQUIRED criterion)
	enable_testing ()

	add_executable (test-pixie
		${pixie_root}/test/test-tagman.c

		${pixie_root}/test/test-9p.h
		${pixie_root}/test/test-9p-pix2.c
		${pixie_root}/test/test-9p-pixS.c
		${pixie_root}/test/test-9p-t-version.c
		${pixie_root}/test/test-9p-r-version.c
		${pixie_root}/test/test-9p-t-attach.c
		${pixie_root}/test/test-9p-t-auth.c
		${pixie_root}/test/test-9p-r-error.c
	)
	target_include_directories (
		test-pixie
		PRIVATE
		${pixie_INCLUDE_DIRS}
		${freebsd_INCLUDE_DIRS}
		${criterion_INCLUDE_DIRS}
	)
	target_link_libraries (
		test-pixie
		${pixie_LIBRARIES}
		${freebsd_LIBRARIES}
		${criterion_LIBRARIES}
	)
	add_test (test-pixie test-pixie)
endif ()

if (BUILD_PIXCHAT)
	pkg_check_modules (guile REQUIRED guile-2.2)

	set (guile_snarf_flags)
	list (APPEND guile_snarf_flags ${CMAKE_C_FLAGS})
	foreach (dir	${pixie_INCLUDE_DIRS}
			${freebsd_INCLUDE_DIRS}
			${guile_INCLUDE_DIRS})
		list (APPEND guile_snarf_flags " -I${dir}")
	endforeach ()
	string (REGEX REPLACE " +" ";" guile_snarf_flags ${guile_snarf_flags})

	set (CMAKE_CPP "${CMAKE_C_COMPILER} -E")
	add_custom_command (
		OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/pixchat-snarf.x
		DEPENDS ${pixie_root}/src/pixchat.c
		COMMENT "Snarfing Guile objects from src/pixchat.c..."
		WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
		COMMAND_EXPAND_LISTS
		VERBATIM
		COMMAND
			env CPP=${CMAKE_CPP}
			guile-snarf
			-o ${CMAKE_CURRENT_BINARY_DIR}/pixchat-snarf.x
			${guile_snarf_flags}
			-Wno-macro-redefined
			${pixie_root}/src/pixchat.c
	)

	add_executable (pixchat
		pixchat-snarf.x
		${pixie_root}/src/pixchat.c
	)
	target_include_directories (
		pixchat
		PRIVATE
		${pixie_INCLUDE_DIRS}
		${CMAKE_CURRENT_BINARY_DIR}
		${freebsd_INCLUDE_DIRS}
		${guile_INCLUDE_DIRS}
	)
	target_link_libraries (
		pixchat
		${pixie_LIBRARIES}
		${freebsd_LIBRARIES}
		${guile_LIBRARIES}
	)
endif ()

