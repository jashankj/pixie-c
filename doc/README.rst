////////////////////////////////////////////////////////////////////////
                                What's here?
////////////////////////////////////////////////////////////////////////

``9pfbl/``:
   a copy of ``//plan9/sys/man/5/``
   from the last Plan 9 From Bell Labs release.

``9pext/``:
   a copy of ``//plan9-contrib/sys/man/5/``
   from one of the maintained Plan 9 forks;
   it's mostly identical to 9pfbl.

``9pfus/``:
   a copy of ``//plan9port/man/man9/``
   from Plan 9 From User Space.

``rfc-9p2000*``:
   copies of the draft IETF-style RFCs
   developed by Eric Van Hensbergen et al
   describing the 9P2000 protocol family.

``diod-protocol.md``:
   a copy of ``//diod/protocol.md``,
   which is now, more or less,
   the authoritative 9P2000.L reference.

``set-9man``:
   a very horrible shell script
   that runs off Plan 9 man pages as PDFs;
   requires lots of userland setup!
