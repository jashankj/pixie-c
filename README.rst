========================================================================
            pixie: yet another 9P2000 protocol implementation
========================================================================

:Version:       0.1.0
:Build (trunk):
   .. image::   https://gitlab.com/jashankj/pixie-c/badges/trunk/pipeline.svg
      :target:  https://gitlab.com/jashankj/pixie-c/commits/trunk
      :alt:     pipeline status for trunk
   .. image::   https://gitlab.com/jashankj/pixie-c/badges/trunk/coverage.svg
      :target:  https://gitlab.com/jashankj/pixie-c/commits/trunk
      :alt:     coverage report for trunk
:SPDX-License-Identifier: BSD-2-Clause
:License:
   Copyright 2021-22, The University of New South Wales, Australia.

   This software may be distributed and modified according to the terms
   of the BSD 2-Clause license.  Note that NO WARRANTY is provided.
   See "LICENSES/BSD-2-Clause.txt" for details.

   Portions derived from the Plan 9 from Bell Labs distribution,
   which is MIT licensed by the Plan 9 Foundation.
   See "LICENSES/MIT.txt" for details.

:Authors:
   - Jashank Jeremy <jashank.jeremy@unsw.edu.au>


Another 9P implementation.  Work-in-progress.

Build::

  $ cmake -S . -B obj -D CMAKE_C_COMPILER=clang
  $ cmake --build obj
