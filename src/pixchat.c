/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2022 The University of New South Wales, Australia.
 */

/**
 * @file  pixchat.c
 * @brief a Guile instance with pixie hooks
 *
 * Some (but probably not enough) magic to make Guile speak to libpixie;
 * and, therefore, to allow prodding state from an executive.
 *
 *
 * Implementation notes:
 *
 * + We use the Guile Magic Snarfer (`guile-snarf').
 */

#include <assert.h>
#include <err.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <libguile.h>
#include <libguile/snarf.h>

#include <bsd/sys/cdefs.h>
#include <bsd/sys/param.h>
#include <bsd/sys/types.h>
#include <bsd/sys/sbuf.h>

#include "pixie.h"
#include "pixie-proto.h"
#include "pixie-raw.h"
#include "pixie-tagman.h"

////////////////////////////////////////////////////////////////////////

#pragma clang diagnostic ignored "-Wincompatible-function-pointer-types"

#define make_typesyn_to_from(type, base) \
	static inline SCM \
	scm_from_##type (type it) \
	{ \
		return scm_from_##base (it); \
	} \
	\
	static inline type \
	scm_to_##type (SCM it) \
	{ \
		return scm_to_##base (it); \
	}

#define make_structp_to_from(t) \
	static inline SCM \
	scm_from_##t (struct t *it, void (*dtor)(void *)) \
	{ \
		return scm_from_pointer (it, dtor); \
	} \
	\
	static inline struct t * \
	scm_to_##t (SCM it) \
	{ \
		return scm_to_pointer(it); \
	}

make_structp_to_from (pix_pcb)
make_structp_to_from (pix_raw)
make_structp_to_from (pix_tagman)
make_typesyn_to_from (pixfid, uint32)
make_typesyn_to_from (pixtag, uint16)
make_typesyn_to_from (pix8,   uint64)
make_typesyn_to_from (pix4,   uint32)
make_typesyn_to_from (pix2,   uint16)
make_typesyn_to_from (pix1,   uint8)

////////////////////////////////////////////////////////////////////////

static const SCM Qnil = SCM_EOL; // i guess?

////////////////////////////////////////////////////////////////////////

#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	SCM_KEYWORD(k_##Xxxx, #Xxxx);
#define F(x_xxx, fieldty, fieldname) \
	SCM_KEYWORD(k_##fieldname, #fieldname);
#include "pixie-msgtypes.inc"

#define O(t,k)					\
	SCM_KEYWORD(k_##k, #k);
O(pix2,   size)
O(pix2,   type)
O(pix4,   dev)
O(pixqid, qid)
O(pix4,   mode)
O(pix4,   atime)
O(pix4,   atime)
O(pix4,   mtime)
O(pix8,   length)
O(pixS,   name)
O(pixS,   uid)
O(pixS,   gid)
O(pixS,   muid)
#undef O

SCM_KEYWORD (scm_NOTAG, "NOTAG");
SCM_KEYWORD (scm_NOFID, "NOFID");

SCM_KEYWORD (scm_9P2000,   "9P2000");
SCM_KEYWORD (scm_9P2000_u, "9P2000_u");
SCM_KEYWORD (scm_9P2000_L, "9P2000_L");

////////////////////////////////////////////////////////////////////////

static inline enum pix_type
scm_to_pix_type (SCM type)
{
	if (! scm_is_keyword (type))
		scm_wrong_type_arg (__func__, 0, type);

#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	if (scm_is_true (scm_eq_p (k_##Xxxx, type))) \
		return X_XXX;
#include "pixie-msgtypes.inc"

	scm_misc_error (__func__, "unknown `pix_type' value", type);
}

static inline SCM
scm_from_pixqid (pixqid it)
{
	return scm_list_3 (
		scm_from_pix1 (it.qtype),
		scm_from_pix4 (it.qver),
		scm_from_pix8 (it.qpath)
	);
}

static inline pixqid
scm_to_pixqid (SCM it)
{
	if (scm_ilength (it) != 3)
		scm_wrong_type_arg (__func__, 0, it);
	return (pixqid) {
		.qtype = scm_to_pix1 (SCM_CAR (it)),
		.qver  = scm_to_pix4 (SCM_CDAR (it)),
		.qpath = scm_to_pix8 (SCM_CDDAR (it))
	};
}


static inline pixS
scm_to_pixS (SCM it)
{
	size_t s   = scm_to_size_t (scm_string_length (it));
	char *text = strndup (scm_to_utf8_string (it), s);
	return (pixS){ .s = s, .text = (pix1 *) text };
}

static inline pixdent
scm_to_pixdent (SCM it)
{
	pixdent out;
#define O(t,k) \
	out.k = scm_to_##t (scm_assq_ref (it, k_##k));

	O(pix2,   size)
	O(pix2,   type)
	O(pix4,   dev)
	O(pixqid, qid)
	O(pix4,   mode)
	O(pix4,   atime)
	O(pix4,   atime)
	O(pix4,   mtime)
	O(pix8,   length)
	O(pixS,   name)
	O(pixS,   uid)
	O(pixS,   gid)
	O(pixS,   muid)
#undef O
	return out;
}

static inline pix1A scm_to_pix1A (SCM it) { assert(!"unimplemented"); }
static inline pixSA scm_to_pixSA (SCM it) { assert(!"unimplemented"); }
static inline pixqidA scm_to_pixqidA (SCM it) { assert(!"unimplemented"); }

////////////////////////////////////////////////////////////////////////

static void *
memdup (void *that, size_t len)
{
	void *this = malloc (len);
	memcpy (this, that, len);
	return this;
}

////////////////////////////////////////////////////////////////////////

SCM_DEFINE (
	scm_pix_tagman_new, "pix-tagman-new", 0, 0, 0,
	(void),
	"."
) {
	return scm_from_pix_tagman (
		pix_tagman_new (), &pix_tagman_drop);
}

SCM_DEFINE (
	scm_pix_tagman_size, "pix-tagman-size", 1, 0, 0,
	(SCM tagman),
	"."
) {
	return scm_from_size_t (
		pix_tagman_size (scm_to_pix_tagman (tagman)));
}

SCM_DEFINE (
	scm_pix_tagman_alloc, "pix-tagman-alloc", 1, 0, 0,
	(SCM tagman),
	"."
) {
	return scm_from_pixtag (
		pix_tagman_alloc (scm_to_pix_tagman (tagman)));
}

SCM_DEFINE (
	scm_pix_tagman_free, "pix-tagman-free", 2, 0, 0,
	(SCM tagman, SCM tag),
	"."
) {
	pix_tagman_free (scm_to_pix_tagman (tagman), scm_to_pixtag (tag));
	return Qnil;
}

////////////////////////////////////////////////////////////////////////

SCM_DEFINE (
	scm_pix_raw_new, "pix-raw-new", 2, 0, 1,
	(SCM type, SCM tag, SCM rest),
	"."
) {
	struct pix_raw it;
	it.type = scm_to_pix_type (type);

	if (scm_keyword_p (tag) && scm_eq_p (tag, scm_NOTAG))
		it.tag = NOTAG;
	else
		it.tag = scm_to_pixtag (tag);

	SCM
		afid,
		aname,
		aqid,
		count,
		data,
		ename,
		errno,
		fid,
		iounit,
		mode,
		msize,
		name,
		newfid,
		nstat,
		nwname,
		nwqid,
		offset,
		oldtag,
		perm,
		qid,
		stat,
		uname,
		version,
		wname,
		wqid;

	switch (it.type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX:
#define FS(x_xxx) \
		scm_c_bind_keyword_arguments ( \
			"pix-raw-new/" #x_xxx, \
			rest, \
			0
#define F(x_xxx, fieldty, fieldname) \
			, k_##fieldname, &fieldname
#define FE(x_xxx) \
		); break;
#include "pixie-msgtypes.inc"

	default:
		scm_misc_error (__func__, "unrecognised type", Qnil);
		return Qnil;
	}

	switch (it.type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX:
#define F(x_xxx, fieldty, fieldname) \
		it.x_xxx.fieldname = scm_to_##fieldty (fieldname);
#define FE(x_xxx) \
		break;
#include "pixie-msgtypes.inc"

	default:
		scm_misc_error (__func__, "unrecognised type", Qnil);
		return Qnil;
	}

	it.size = pix_raw_size (&it);
	struct pix_raw *out = memdup (&it, sizeof it);
	return scm_from_pix_raw (out, free);
}

SCM_DEFINE (
	scm_pix_raw_decode, "pix-raw-decode", 1, 1, 0,
	(SCM s_bv, SCM s_dialect),
	"."
) {
	if (! scm_is_bytevector (s_bv))
		scm_wrong_type_arg (__func__, 0, s_bv);
	pixV dialect = PIX_9P2000;
	if (! SCM_UNBNDP (s_dialect)) {
		if (scm_eq_p (s_dialect, scm_9P2000))
			dialect = PIX_9P2000;
		else if (scm_eq_p (s_dialect, scm_9P2000_u))
			dialect = PIX_9P2000_u;
		else if (scm_eq_p (s_dialect, scm_9P2000_L))
			dialect = PIX_9P2000_L;
		else
			scm_wrong_type_arg (__func__, 1, s_dialect);
	}

	struct pix_raw that;
	if (pix_raw_decode (
			(const uint8_t *) SCM_BYTEVECTOR_CONTENTS (s_bv),
			(size_t) SCM_BYTEVECTOR_LENGTH (s_bv),
			dialect, &that) < 0)
		scm_out_of_range_pos ("pix-raw-decode", s_bv, 0);

	struct pix_raw *result = memdup (&that, sizeof that);
	return scm_from_pix_raw (result, free);
}

SCM_DEFINE (
	scm_pix_raw_encode, "pix-raw-encode", 1, 0, 0,
	(SCM smsg),
	"."
) {
	uint8_t *buf; size_t len;
	struct pix_raw *msg = scm_to_pix_raw (smsg);
	if (pix_raw_encode (msg, &buf, &len) < 0)
		scm_misc_error (__func__, "bad message", smsg);

	SCM bv = scm_c_make_bytevector (len);
	memcpy (SCM_BYTEVECTOR_CONTENTS (bv), buf, len);
	free (buf);
	return bv;
}

SCM_DEFINE (
	scm_pix_raw_size, "pix-raw-size", 1, 0, 0,
	(SCM msg),
	"."
) {
	return scm_from_size_t (pix_raw_size (scm_to_pix_raw (msg)));
}

SCM_DEFINE (
	scm_pix_raw_fmt, "pix-raw-fmt", 1, 0, 0,
	(SCM smsg),
	"."
) {
	struct sbuf *buf = sbuf_new_auto ();
	struct pix_raw *msg = scm_to_pix_raw (smsg);
	pix_raw_fmt (msg, buf);
	sbuf_finish (buf);
	SCM out = scm_from_utf8_string (scm_strdup (sbuf_data (buf)));
	sbuf_delete (buf);
	return out;
}

////////////////////////////////////////////////////////////////////////

SCM_DEFINE (
	scm_pix_proto_new, "pix-proto-new", 0, 0, 0,
	(void),
	"."
) {
	struct pix_pcb *pcb = pix_proto_new ();
	return scm_from_pix_pcb (pcb, &pix_proto_drop);
}

SCM_DEFINE (
	scm_pix_proto_set_dialect, "pix-proto-set-dialect", 2, 0, 0,
	(SCM s_pcb, SCM s_mask),
	"."
) {
	struct pix_pcb *pcb = scm_to_pix_pcb (s_pcb);
	unsigned mask = scm_to_uint (s_mask);
	int result = pix_proto_set_dialect (pcb, mask);
	return scm_from_int (result);
}

SCM_DEFINE (
	scm_pix_proto_recv_bytes, "pix-proto-recv-bytes", 2, 0, 0,
	(SCM s_pcb, SCM s_bv),
	"."
) {
	struct pix_pcb *pcb = scm_to_pix_pcb (s_pcb);
	if (! scm_is_bytevector (s_bv))
		scm_wrong_type_arg (__func__, 0, s_bv);
	const uint8_t *buf = (const uint8_t *) SCM_BYTEVECTOR_CONTENTS (s_bv);
	size_t buflen = SCM_BYTEVECTOR_LENGTH (s_bv);
	if (pix_proto_recv_bytes (pcb, buf, buflen) < 0)
		scm_misc_error(__func__, "pix-proto-recv-bytes", s_bv);
	return Qnil;
}

SCM_DEFINE (
	scm_pix_proto_recv_msg, "pix-proto-recv-msg", 2, 0, 0,
	(SCM s_pcb, SCM s_msg),
	"."
) {
	struct pix_pcb *pcb = scm_to_pix_pcb (s_pcb);
	struct pix_raw *msg = scm_to_pix_raw (s_msg);
	if (pix_proto_recv_msg (pcb, msg) < 0)
		scm_misc_error(__func__, "pix-proto-recv-msg", s_msg);
	return Qnil;
}

////////////////////////////////////////////////////////////////////////

static void
main_1 (void *closure, int argc, char **argv)
{
	scm_c_use_module ("ice-9 readline");
	SCM Factivate_readline = scm_c_public_lookup (
		"ice-9 readline", "activate-readline");

	scm_c_use_module ("rnrs bytevectors");

	scm_call_0 (scm_variable_ref (Factivate_readline));

#ifndef SCM_MAGIC_SNARFER
#include "pixchat-snarf.x"
#endif

	scm_shell (argc, argv);
	/* after exit */
}

int
main (int argc, char **argv)
{
	scm_boot_guile (argc, argv, main_1, 0);
	return 0; /* never reached, see inner_main */
}
