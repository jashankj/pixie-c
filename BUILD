#!/bin/sh
#-
# Copyright 2021-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

clang="${clang-clang}"
clang_rt_dir=$($clang -print-resource-dir)/lib/$(uname | tr A-Z a-z)

cflags=""
ldflags=""

cflags="${cflags} -g"
cflags="${cflags} -fno-omit-frame-pointer"
cflags="${cflags} -fno-optimize-sibling-calls"

# AddressSanitizer
cflags="${cflags} -fsanitize=address"
cflags="${cflags} -fsanitize-address-use-after-scope"
cflags="${cflags} -fsanitize-address-use-after-return=always"
ldflags="${ldflags} -shared-libasan"
ldflags="${ldflags} -Wl,-rpath,$clang_rt_dir"

# UndefinedBehaviourSanitizer
cflags="${cflags} -fsanitize=undefined"

# CFI would be nice, but `-fvisibility=hidden' breaks Criterion.
# cflags="${cflags} -fsanitize=cfi"
# cflags="${cflags} -fno-sanitize-trap=cfi"
# cflags="${cflags} -fsanitize-stats"
# cflags="${cflags} -fvisibility=hidden"
# cflags="${cflags} -flto"
ldflags="${ldflags} -fuse-ld=lld"

# Use SanCov to do coverage.
# cflags="${cflags} -fsanitize-coverage=edge"
# cflags="${cflags} -fsanitize-coverage=trace-pc-guard"
# cflags="${cflags} -fsanitize-coverage=indirect-calls"
# cflags="${cflags} -fsanitize-coverage=trace-cmp"
# cflags="${cflags} -fsanitize-coverage=trace-div"
# cflags="${cflags} -fsanitize-coverage=trace-gep"
# # cflags="${cflags} -fsanitize-coverage=trace-loads"
# # cflags="${cflags} -fsanitize-coverage=trace-stores"

# Use gcov-alike for coverage.
cflags="${cflags} -fprofile-arcs"
cflags="${cflags} -ftest-coverage"

exec cmake \
	-G Ninja \
	-S . \
	-B obj \
	-D CMAKE_BUILD_TYPE=Debug \
	-D CMAKE_C_COMPILER="${clang}" \
	-D CMAKE_C_FLAGS="${cflags}" \
	-D CMAKE_EXE_LINKER_FLAGS="${ldflags}" \
	-D CMAKE_EXPORT_COMPILE_COMMANDS=1 \
	"$@"
