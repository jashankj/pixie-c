#-
# Copyright 2020-22, The University of New South Wales, Australia.
# SPDX-License-Identifier: BSD-2-Clause
#

cmake_minimum_required (VERSION 3.0.0)
project (zf_log
	VERSION		2019.08.07
	DESCRIPTION	"core logging library for C, C++, Obj-C"
	HOMEPAGE_URL	"<https://github.com/wonder-mice/zf_log>"
	LANGUAGES	C
)

set (zf_log_root extern/zf_log)

add_library (zf_log STATIC
	${zf_log_root}/zf_log/zf_log.h
	${zf_log_root}/zf_log/zf_log.c
)

set_property (TARGET zf_log PROPERTY C_STANDARD 99)
set_property (TARGET zf_log PROPERTY C_STANDARD_REQUIRED ON)
#set_property (TARGET zf_log PROPERTY C_EXTENSIONS OFF)
target_compile_options (zf_log PRIVATE -Wall -Wextra -pedantic-errors)

# Pick up our configuration file.
# target_compile_definitions (zf_log PRIVATE -DZF_LOG_USE_CONFIG_HEADER)
# target_include_directories (zf_log PRIVATE include/zf_log)

target_include_directories (zf_log PUBLIC
	${zf_log_root}/zf_log
)
