/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-flush.c
 * @brief 9P implementation: 9P2000 Rflush message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_flush)
{
	return 0;
}

SER(pix_9p_r_flush)
{
	SER_PRE (size (pix_9p_r_flush, from));

	return (ptrdiff_t) size (pix_9p_r_flush, from);
}

DE(pix_9p_r_flush)
{
	return 0;
}

FMT(pix_9p_r_flush)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_flush);
	CHECK (writef ("Rflush"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	return 0;
}

EQ(pix_9p_r_flush)
{
	return true;
}

////////////////////////////////////////////////////////////////////////
