/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-clunk.c
 * @brief 9P implementation: 9P2000 Tclunk message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_clunk)
{
	return
		size (pixfid);
}

SER(pix_9p_t_clunk)
{
	SER_PRE (size (pix_9p_t_clunk, from));

	ser (pixfid, &from->fid, into);

	return (ptrdiff_t) size (pix_9p_t_clunk, from);
}

DE(pix_9p_t_clunk)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_clunk out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->fid);

	return total;
}

FMT(pix_9p_t_clunk)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_clunk);
	CHECK (writef ("Tclunk"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid ")); CHECK (fmt (pixfid, &self->fid));
	return 0;
}

EQ(pix_9p_t_clunk)
{
	return
		eq (a, b, pixfid, fid);
}

////////////////////////////////////////////////////////////////////////
