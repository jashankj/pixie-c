/*-
 * SPDX-License-Identifier: BSD-2-Clause AND MIT
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 *
 * Portions derived from the Plan 9 from Bell Labs distribution,
 * which is MIT licensed by the Plan 9 Foundation, and which was formerly
 * Copyright (C) 2003, Lucent Technologies Inc.  and others.  All Rights Reserved.
 * This protocol was the result of work done by the Computing Science
 * Research Center of AT&T Bell Labs (now a part of Lucent Technologies,
 * now a part of Alcatel-Lucent, now a part of Nokia) and in particular:
 *   + Rob Pike
 *   + Dave Presotto
 *   + Ken Thompson
 * Contains code from:
 *   + //plan9/sys/src/libc/9sys/fcallfmt.c
 *   + //plan9/sys/include/fcall.h
 *   + //plan9/sys/include/libc.h
 * Contains documentation from:
 *   + //plan9/sys/man/5/0intro
 *   + //plan9/sys/man/5/attach
 *   + //plan9/sys/man/5/clunk
 *   + //plan9/sys/man/5/error
 *   + //plan9/sys/man/5/flush
 *   + //plan9/sys/man/5/open
 *   + //plan9/sys/man/5/read
 *   + //plan9/sys/man/5/remove
 *   + //plan9/sys/man/5/stat
 *   + //plan9/sys/man/5/version
 *   + //plan9/sys/man/5/walk
 *
 * Portions derived from the draft RFC
 *   "Plan 9 Remote Resource Protocol" (aka "9P2000"),
 * which is copyright (c) 2005 the authors of that document:
 *   + Eric Van Hensbergen, Plan 9 Fans
 * Contains documentation from
 *   + //9p-rfc/9p2000.xml
 *
 * Portions derived from the draft RFC
 *   "Plan 9 Remote Resource Protocol Unix Extensions" (aka "9P2000.u"),
 * which is copyright (c) 2005 the authors of that document:
 *   + Eric Van Hensbergen, Plan 9 Fans
 * Contains documentation from
 *   + //9p-rfc/9p2000.u.xml
 *
 * Portions derived from the draft RFC
 *   "9P2010: Plan 9 Remote Resource Protocol" (aka "9P2000.L"),
 * which is copyright (c) 2010 the authors of that document:
 *   + Eric Van Hensbergen, IBM Research
 * Contains documentation from
 *   + //9p-rfc/9p2000.L.xml
 *   + //9p-rfc/9p2000.L.prelim.txt
 *
 * Portions derived from diod, which is GPL-2.0-or-later licensed, and
 * Copyright 2010 Lawrence Livermore National Security, LLC.
 * That work was produced at the Lawrence Livermore National Laboratory
 * under Contract No. DE-AC52-07NA27344 with the DOE.
 * Contains documentation from
 *   + //diod/protocol.md
 * This is done under duress, as no other compatibly-licensed resource
 * for this material exists.
 *
 * Portions derived from the Linux kernel,
 * which is GPL-2.0-only licensed by the Linux kernel authors.
 * Contains constants defined in:
 *   + //linux/include/net/9p/9p.h
 * This is done under duress, as no other compatibly-licensed resource
 * for this material exists.
 */

/**
 * @file  pixie-msgtypes.inc
 * @brief 9P implementation: raw protocol description tables
 *
 * Resources:
 *
 *  + <https://en.wikipedia.org/wiki/9P_(protocol)>
 *  + <https://9p.cat-v.org/documentation/>
 *  + <https://man.cat-v.org/plan_9/5/>
 *  + <https://wiki.qemu.org/Documentation/9p>
 *  + <https://ericvh.github.io/9p-rfc/rfc9p2000.html>
 *  + <https://ericvh.github.io/9p-rfc/rfc9p2000.u.html>
 *  + <https://ericvh.github.io/9p-rfc/rfc9p2000.L.html>
 *  + <https://github.com/chaos/diod/blob/master/protocol.md>
 *
 *
 * Symbols:
 *
 *   + `O(Xxxx, N, X_XXX, x_xxx, req?, res?, dialect)`
 *     defines a message with 9P name `Xxxx`, type number N, and
 *     inflected names `X_XXX` and `x_xxx`, which is defined in
 *     any of the protocol dialects in the bitset `dialect`.
 *
 *   + `FS(x_xxx)`
 *     denotes the start of the field list for message `x_xxx`.
 *
 *   + `F(x_xxx, fieldtype, fieldname)`
 *     defines a field in message `x_xxx` of type `fieldtype`
 *     and with name `fieldname`
 *
 *   + `FE(x_xxx)`
 *     denotes the end of the field list for message `x_xxx`.
 *
 *
 * Notes:
 *
 * + 9P2000.u makes the obnoxious choice to change message layouts
 *   without allocating new message types --- i.e., a message type value
 *   in 9P will have the same over-the-wire bits set as a 9P2000.u
 *   message, even though the message will be structurally different.
 *   For example, Rerror grows a field in 9P2000.u mode.
 */

#ifndef O
#error pixie-msgtypes.inc included without "O" defined!
#endif

#ifndef FS
#define FS(x_xxx)
#endif

#ifndef F
#define F(x_xxx, fieldty, fieldname)
#endif

#ifndef FE
#define FE(x_xxx)
#endif

/**
 * @page	9P	The 9P Remote Resource Protocol
 * @tableofcontents
 */

/**
 * @page	9P
 * @section	p9-version	version: negotiate protocol version
 *
 * Description
 * ------------------------------------
 *
 * The **version** request negotiates the protocol version and message
 * size to be used on the connection and initializes the connection for
 * I/O.  **Tversion** must be the first message sent on the 9P
 * connection, and the client cannot issue any further requests until it
 * has received the **Rversion** reply.  The *tag* should be **NOTAG**
 * (value `(ushort)~0`) for a **version** message.
 *
 * The client suggests a maximum message size, *msize*, that is the
 * maximum length, in bytes, it will ever generate or expect to receive
 * in a single 9P message.  This count includes all 9P protocol data,
 * starting from the *size* field and extending through the message, but
 * excludes enveloping transport protocols.  The server responds with
 * its own maximum, *msize*, which must be less than or equal to the
 * client's value.  Thenceforth, both sides of the connection must honor
 * this limit.
 *
 * The *version* string identifies the level of the protocol.
 * The string must always begin with the two characters `"9P"`.
 * If the server does not understand the client's version string,
 * it should respond with an **Rversion** message (not **Rerror**)
 * with the *version* string the 7 characters `"unknown"`.
 *
 * The server may respond with the client's version string, or a version
 * string identifying an earlier defined protocol version.  Currently,
 * the only defined version is the 6 characters `"9P2000"`.
 *
 * Version strings are defined such that, if the client string contains
 * one or more period characters, the initial substring up to but not
 * including any single period in the version string defines a version
 * of the protocol.  After stripping any such period-separated suffix,
 * the server is allowed to respond with a string of the form
 * <b>9P<i>nnnn</i></b>, where *nnnn* is less than or equal to the
 * digits sent by the client.
 *
 * The client and server will use the protocol version defined by the
 * server's response for all subsequent communication on the connection.
 *
 * A successful **version** request initializes the connection.  All
 * outstanding I/O on the connection is aborted; all active fids are
 * freed ('clunked') automatically.  The set of messages between
 * **version** requests is called a *session*.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * The **version** message is generated by the **fversion** system
 * call. It is also generated automatically, if required, by a **mount**
 * or **fauth** system call on an uninitialized connection.
 *
 */

O(Tversion,	100,	T_VERSION,	t_version,	1, 0,	PIX_9P2000 | PIX_9P2000_u)
FS(t_version)
	F(t_version,	pix4,	msize)
	F(t_version,	pixS,	version)
FE(t_version)

O(Rversion,	101,	R_VERSION,	r_version,	0, 1,	PIX_9P2000 | PIX_9P2000_u)
FS(r_version)
	F(r_version,	pix4,	msize)
	F(r_version,	pixS,	version)
FE(r_version)


/**
 * @page	9P
 * @section	p9-attach	attach, auth: messages to establish a connection
 *
 * Description
 * ------------------------------------
 *
 * The **attach** message serves as a fresh introduction from a user on
 * the client machine to the server.  The message identifies the user
 * (*uname*) and may select the file tree to access (*aname*).  The
 * *afid* argument specifies a fid previously established by an **auth**
 * message, as described below.
 *
 * As a result of the **attach** transaction, the client will have a
 * connection to the root directory of the desired file tree,
 * represented by *fid*.  An error is returned if *fid* is already in
 * use.  The server's idea of the root of the file tree is represented
 * by the returned *qid*.
 *
 * If the client does not wish to authenticate the connection,
 * or knows that authentication is not required, the *afid* field
 * in the **attach** message should be set to **NOFID**, defined as
 * `(u32int)~0` in `<fcall.h>`.  If the client does wish to
 * authenticate, it must acquire and validate an *afid* using an
 * **auth** message before doing the *attach**.
 *
 * The **auth** message contains *afid*, a new fid to be established for
 * authentication, and the *uname* and *aname* that will be those of the
 * following **attach** message.  If the server does not require
 * authentication, it returns **Rerror** to the **Tauth** message.
 *
 * If the server does require authentication,
 * it returns *aqid* defining a file of type **QTAUTH** that may be read
 * and written (using **read** and **write** messages in the usual way)
 * to execute an authentication protocol.  That protocol's definition is
 * not part of 9P itself.
 *
 * Once the protocol is complete, the same *afid* is presented in the
 * **attach** message for the user, granting entry.  The same validated
 * *afid* may be used for multiple **attach** messages with the same
 * *uname* and *aname*.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * An **attach** transaction will be generated for kernel devices (see
 * *intro(3)*) when a system call evaluates a file name beginning with
 * *Pipe(2)* generates an attach on the kernel device *pipe(3)*.  The
 * *mount* system call (see *bind(2)*) generates an **attach** message
 * to the remote file server.  When the kernel boots, an *attach* is
 * made to the root device, *root(3)* and then an **attach** is made to
 * the requested file server machine.
 *
 * An **auth** transaction is generated by the *fauth(2)* system call or
 * by the first **mount** system call on an uninitialized connection.
 *
 */

O(Tauth,	102,	T_AUTH,		t_auth,		1, 0,	PIX_9P2000 | PIX_9P2000_u)
FS(t_auth)
	F(t_auth,	pixfid,	afid)
	F(t_auth,	pixS,	uname)
	F(t_auth,	pixS,	aname)
FE(t_auth)

O(Rauth,	103,	R_AUTH,		r_auth,		0, 1,	PIX_9P2000 | PIX_9P2000_u)
FS(r_auth)
	F(r_auth,	pixqid,	aqid)
FE(r_auth)

O(Tattach,	104,	T_ATTACH,	t_attach,	1, 0,	PIX_9P2000 | PIX_9P2000_u)
FS(t_attach)
	F(t_attach,	pixfid,	fid)
	F(t_attach,	pixfid,	afid)
	F(t_attach,	pixS,	uname)
	F(t_attach,	pixS,	aname)
FE(t_attach)

O(Rattach,	105,	R_ATTACH,	r_attach,	0, 1,	PIX_9P2000 | PIX_9P2000_u)
FS(r_attach)
	F(r_attach,	pixqid,	qid)
FE(r_attach)


/**
 * @page	9P
 * @section	p9-error	error: return an error
 *
 * Description
 * ------------------------------------
 *
 * The **Rerror** message (there is no **Terror**) is used to return an
 * error string describing the failure of a transaction.  It replaces
 * the corresponding reply message that would accompany a successful
 * call; its tag is that of the failing request.
 *
 * By convention, clients may truncate error messages after **ERRMAX-1**
 * bytes; **ERRMAX** is defined in `<libc.h>`.
 *
 */

O(Rerror,	107,	R_ERROR,	r_error,	0, 1,	PIX_9P2000)
FS(r_error)
	F(r_error,	pixS,	ename)
FE(r_error)

/*
O(URerror,	107,	UR_ERROR,	ur_error,	PIX_9P2000_u)
FS(ur_error)
	F(ur_error,	pixS,	ename)
	F(ur_error,	pix4,	errno)
FE(ur_error)
*/


/**
 * @page	9P
 * @section	p9-flush	flush: abort a message
 *
 * Description
 * ------------------------
 *
 * When the response to a request is no longer needed, such as when a
 * user interrupts a process doing a *read(2)*, a **Tflush** request is
 * sent to the server to purge the pending response.  The message being
 * flushed is identified by *oldflush*.  The semantics of **flush**
 * depends on messages arriving in order.
 *
 * The server should answer the **flush** message immediately.  If it
 * recognizes *oldtag* as the tag of a pending transaction, it should
 * abort any pending response and discard that tag.  In either case, it
 * should respond with an **Rflush** echoing the *tag* (not *oldtag*) of
 * the **Tflush** message.  A **Tflush** can never be responded to by an
 * **Rerror** message.
 *
 * The server may respond to the pending request before responding to
 * the **Tflush**.  It is possible for a client to send multiple
 * **Tflush** messages for a particular pending request.  Each
 * subsequent **Tflush** must contain as *oldtag* the tag of the pending
 * request (not a previous **Tflush**.  Should multiple **Tflush** es
 * be received for a pending request, they must be answered in order.
 * A **Rflush** for any of the multiple **Tflush** es implies an answer
 * for all previous ones.  Therefore, should a server receive a request
 * and then multiple flushes for that request, it need respond only to
 * the last flush.
 *
 * When the client sends a **Tflush**, it must wait to receive the
 * corresponding **Rflush** before reusing *oldtag* for subsequent
 * messages.  If a response to the flushed request is received before
 * the **Rflush**, the client must honor the response as if it had not
 * been flushed, since the completed request may signify a state change
 * in the server.  For instance, **Tcreate** may have created a file and
 * **Twalk** may have allocated a fid.  If no response is received
 * before the **Rflush**, the flushed transaction is considered to have
 * been canceled, and should be treated as though it had never been
 * sent.
 *
 * Several exceptional conditions are handled correctly by the above
 * specification: sending multiple flushes for a single tag, flushing
 * after a transaction is completed, flushing a **Tflush**, and flushing
 * an invalid tag.
 *
 */

O(Tflush,	108,	T_FLUSH,	t_flush,	1, 0,	PIX_9P2000 | PIX_9P2000_u)
FS(t_flush)
	F(t_flush,	pixtag,	oldtag)
FE(t_flush)

O(Rflush,	109,	R_FLUSH,	r_flush,	0, 1,	PIX_9P2000)
FS(r_flush)
FE(r_flush)


/**
 * @page	9P
 * @section	p9-walk	walk: descend a directory hierarchy
 *
 * Description
 * ------------------------
 *
 * The **walk** request carries as arguments an existing *fid* and a
 * proposed *newfid* (which must not be in use unless it is the same as
 * *fid*) that the client wishes to associate with the result of
 * traversing the directory hierarchy by 'walking' the hierarchy using
 * the successive path name elements *wname*.  The *fid* must represent
 * a directory unless zero path name elements are specified.
 *
 * The *fid* must be valid in the current session and must not have been
 * opened for I/O by an **open** or **create** message.  If the full
 * sequence of *nwname* elements is walked successfully, *newfid* will
 * represent the file that results.  If not, *newfid* (and *fid*) will
 * be unaffected.  However, if *newfid* is in use or otherwise illegal,
 * an **Rerror** is returned.
 *
 * The name **".."** (dot-dot) represents the parent directory.
 * The name **"."** (dot), meaning the current directory,
 * is not used in the protocol.
 *
 * It is legal for *nwname* to be zero, in which case *newfid* will
 * represent the same file as *fid* and the **walk** will usually
 * succeed; this is equivalent to walking to dot.  The rest of this
 * discussion assumes **nwname** is greater than zero.
 *
 * The *nwname* path name elements *wname* are walked in order,
 * "elementwise".  For the first elementwise walk to succeed, the file
 * identified by *fid* must be a directory, and the implied user of the
 * request must have permission to search the directory (see
 * *intro(5)*).  Subsequent elementwise walks have equivalent
 * restrictions applied to the implicit fid that results from the
 * preceding elementwise walk.
 *
 * If the first element cannot be walked for any reason, **Rerror** is
 * returned.  Otherwise, the walk will return an **Rwalk** message
 * containing *nwqid* qids corresponding, in order, to the files that
 * are visited by the *nwqid* successful elementwise walks; *nwqid* is
 * therefore either *nwname* or the index of the first elementwise walk
 * that failed.  The value of *nwqid* cannot be zero unless *nwname* is
 * zero.  Also, *nwqid* will always be less than or equal to *nwname*.
 * Only if it is equal, however, will *newfid* be affected, in which
 * case *newfid* will represent the file reached by the final
 * elementwise walk requested in the message.
 *
 * A **walk** of the name **".."** in the root directory of a server is
 * equivalent to a walk with no name elements.
 *
 * If *newfid* is the same as *fid*, the above discussion applies, with
 * the obvious difference that if the walk changes the state of
 * *newfid*, it also changes the state of *fid*; and if *newfid* is
 * unaffected, then *fid* is also unaffected.
 *
 * To simplify the implementation of the servers, a maximum of sixteen
 * name elements or qids may be packed in a single message.  This
 * constant is called `MAXWELEM` in *fcall(2)*.  Despite this
 * restriction, the system imposes no limit on the number of elements in
 * a file name, only the number that may be transmitted in a single
 * message.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * A call to *chdir(2)* causes a **walk**.  One or more **walk**
 * messages may be generated by any of the following calls, which
 * evaluate file names: *bind*, *create*, *exec*, *mount*, *open*,
 * *remove*, *stat*, *unmount*, *wstat*.  The file name element **"."**
 * (dot) is interpreted locally and is not transmitted in **walk**
 * messages.
 *
 */

O(Twalk,	110,	T_WALK,		t_walk,		1, 0,	PIX_9P2000)
FS(t_walk)
	F(t_walk,	pixfid,	fid)
	F(t_walk,	pixfid,	newfid)
	F(t_walk,	pix2,	nwname)
	F(t_walk,	pixSA,  wname)
FE(t_walk)

O(Rwalk,	111,	R_WALK,		r_walk,		0, 1,	PIX_9P2000)
FS(r_walk)
	F(r_walk,	pix2,	nwqid)
	F(r_walk,	pixqidA,wqid)
FE(r_walk)


/**
 * @page	9P
 * @section	p9-open	open, create: prepare a fid for I/O on an existing or new file
 *
 * Description
 * ------------------------

 * The **open** request asks the file server to check permissions and
 * prepare a fid for I/O with subsequent **read** and **write**
 * messages.
 *
 * The *mode* field determines the type of I/O: 0 (called **OREAD** in
 * **<libc.h>**), 1 (**OWRITE**), 2 (**ORDWR**), and 3 (**OEXEC**), mean
 * *read access*, *write access*, *read and write access*, and *execute
 * access*, to be checked against the permissions for the file.
 *
 * In addition, if *mode* has the **OTRUNC** (**0x10**) bit set, the
 * file is to be truncated, which requires write permission (if the file
 * is append-only, and permission is granted, the **open** succeeds but
 * the file will not be truncated); if the *mode* has the **ORCLOSE**
 * (**0x40**) bit set, the file is to be removed when the fid is
 * clunked, which requires permission to remove the file from its
 * directory.  All other bits in *mode* should be zero.
 *
 * It is illegal to write a directory, truncate it, or attempt to remove
 * it on close.  If the file is marked for exclusive use (see
 * *stat(5)*), only one client can have the file open at any time.  That
 * is, after such a file has been opened, further opens will fail until
 * *fid* has been clunked.
 *
 * All these permissions are checked at the time of the **open**
 * request; subsequent changes to the permissions of files do not affect
 * the ability to read, write, or remove an open file.
 *
 * The **create** request asks the file server to create a new file with
 * the *name* supplied, in the directory (*dir*) represented by *fid*,
 * and requires write permission in the directory.  The owner of the
 * file is the implied user id of the request, the group of the file is
 * the same as *dir*, and the permissions are the value of
 *
 *    **`"perm & (~0666 | (dir.perm & 0666))"`**
 *
 * if a regular file is being created and
 *
 *    **`"perm & (~0777 | (dir.perm & 0777))"`**
 *
 * if a directory is being created.  This means, for example, that if
 * the **create** allows read permission to others, but the containing
 * directory does not, then the created file will not allow others to
 * read the file.
 *
 * Finally, the newly created file is opened according to *mode*, and
 * *fid* will represent the newly opened file.  *Mode* is not checked
 * against the permissions in *perm*.  The *qid* for the new file is
 * returned with the **create** reply message.
 *
 * Directories are created by setting the **DMDIR** bit (**0x80000000**)
 * in the *perm*.
 *
 * The names **"."** and **".."** are special; it is illegal to create
 * files with these names.
 *
 * It is an error for either of these messages if the fid is already the
 * product of a successful **open** or **create** message.
 *
 * An attempt to **create** a file in a directory where the given *name*
 * already exists will be rejected; in this case, the *create* system
 * call (see *open(2)*) uses **open** with truncation.  The algorithm
 * used by the *create* system call is: first walk to the directory to
 * contain the file.  If that fails, return an error.  Next **walk** to
 * the specified file.  If the **walk** succeeds, send a request to
 * **open** and truncate the file and return the result, successful or
 * not.  If the **walk** fails, send a create message.  If that fails,
 * it may be because the file was created by another process after the
 * previous walk failed, so (once) try the **walk** and **open** again.
 *
 * For the behavior of *create* on a union directory, see *bind(2)*.
 *
 * The **iounit** field returned by **open** and **create** may be zero.
 * If it is not, it is the maximum number of bytes that are guaranteed
 * to be read from or written to the file without breaking the I/O
 * transfer into multiple 9P messages; see *read(5)*.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * *Open* and *create* both generate **open** messages; only *create*
 * generates a **create** message.  The **iounit** associated with an
 * open file may be discovered by calling *iounit(2)*.
 *
 * For programs that need atomic file creation, without the race that
 * exists in the **open**-create sequence described above, the kernel
 * does the following.  If the **OEXCL** (**0x1000**) bit is set in the
 * *mode* for a **create** system call, the **open** message is not
 * sent; the kernel issues only the **create**.  Thus, if the file
 * exists, **create** will draw an error, but if it doesn't and the
 * **create** system call succeeds, the process issuing the **create**
 * is guaranteed to be the one that created the file.
 *
 */

O(Topen,	112,	T_OPEN,		t_open,		1, 0,	PIX_9P2000)
FS(t_open)
	F(t_open,	pixfid,	fid)
	F(t_open,	pix1,	mode)
FE(t_open)

O(Ropen,	113,	R_OPEN,		r_open,		0, 1,	PIX_9P2000)
FS(r_open)
	F(r_open,	pixqid,	qid)
	F(r_open,	pix4,	iounit)
FE(r_open)

O(Tcreate,	114,	T_CREATE,	t_create,	1, 0,	PIX_9P2000)
FS(t_create)
	F(t_create,	pixfid,	fid)
	F(t_create,	pixS,	name)
	F(t_create,	pix4,	perm)
	F(t_create,	pix1,	mode)
FE(t_create)

O(Rcreate,	115,	R_CREATE,	r_create,	0, 1,	PIX_9P2000)
FS(r_create)
	F(r_create,	pixqid,	qid)
	F(r_create,	pix4,	iounit)
FE(r_create)


/**
 * @page	9P
 * @section	p9-read	read, write: transfer data from and to a file
 *
 * Description
 * ------------------------
 *
 * The **read** request asks for *count* bytes of data from the file
 * identified by *fid*, which must be opened for reading, starting
 * *offset* bytes after the beginning of the file.  The bytes are
 * returned with the **read** reply message.
 *
 * The *count* field in the reply indicates the number of bytes
 * returned.  This may be less than the requested amount.  If the
 * *offset* field is greater than or equal to the number of bytes in the
 * file, a count of zero will be returned.
 *
 * For directories, **read** returns an integral number of directory
 * entries exactly as in **stat** (see *stat(5)*), one for each member
 * of the directory.  The **read** request message must have **offset**
 * equal to zero or the value of **offset** in the previous **read** on
 * the directory, plus the number of bytes returned in the previous
 * **read**.  In other words, seeking other than to the beginning is
 * illegal in a directory (see *seek(2)*).
 *
 * The **write** request asks that *count* bytes of data be recorded in
 * the file identified by *fid*, which must be opened for writing,
 * starting *offset* bytes after the beginning of the file.  If the file
 * is append-only, the data will be placed at the end of the file
 * regardless of *offset*.  Directories may not be written.
 *
 * The **write** reply records the number of bytes actually written.  It
 * is usually an error if this is not the same as requested.
 *
 * Because 9P implementations may limit the size of individual messages,
 * more than one message may be produced by a single *read* or *write*
 * call.  The *iounit* field returned by *open(5)*, if non-zero, reports
 * the maximum size that is guaranteed to be transferred atomically.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * **Read** and **write** messages are generated by the corresponding
 * calls.  Because they include an offset, the *pread* and *pwrite*
 * calls correspond more directly to the 9P messages.  Although
 * *seek(2)* affects the offset, it does not generate a message.
 *
 */

O(Tread,	116,	T_READ,		t_read,		1, 0,	PIX_9P2000)
FS(t_read)
	F(t_read,	pixfid,	fid)
	F(t_read,	pix8,	offset)
	F(t_read,	pix4,	count)
FE(t_read)

O(Rread,	117,	R_READ,		r_read,		0, 1,	PIX_9P2000)
FS(r_read)
	F(r_read,	pix4,	count)
	F(r_read,	pix1A,	data)
FE(r_read)

O(Twrite,	118,	T_WRITE,	t_write,	1, 0,	PIX_9P2000)
FS(t_write)
	F(t_write,	pixfid,	fid)
	F(t_write,	pix8,	offset)
	F(t_write,	pix4,	count)
	F(t_write,	pix1A,	data)
FE(t_write)

O(Rwrite,	119,	R_WRITE,	r_write,	0, 1,	PIX_9P2000)
FS(r_write)
	F(r_write,	pix4,	count)
FE(r_write)


/**
 * @page	9P
 * @section	p9-clunk	clunk: forget about a fid
 *
 * Description
 * ------------------------
 *
 * The **clunk** request informs the file server that the current file
 * represented by *fid* is no longer needed by the client.  The actual
 * file is not removed on the server unless the fid had been opened with
 * **ORCLOSE**.
 *
 * Once a fid has been clunked, the same fid can be reused in a new
 * **walk** or **attach** request.
 *
 * Even if the **clunk** returns an error, the *fid* is no longer valid.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * A **clunk** message is generated by *close* and indirectly by other
 * actions such as failed *open* calls.
 *
 */

O(Tclunk,	120,	T_CLUNK,	t_clunk,	1, 0,	PIX_9P2000)
FS(t_clunk)
 	F(t_clunk,	pixfid,	fid)
FE(t_clunk)

O(Rclunk,	121,	R_CLUNK,	r_clunk,	0, 1,	PIX_9P2000)
FS(r_clunk)
FE(r_clunk)


/**
 * @page	9P
 * @section	p9-remove	remove: remove a file from a server
 *
 * Description
 * ------------------------
 *
 * The **remove** request asks the file server both to remove the file
 * represented by *fid* and to **clunk** the *fid*, even if the remove
 * fails.  This request will fail if the client does not have write
 * permission in the parent directory.
 *
 * It is correct to consider **remove** to be a **clunk** with the side
 * effect of removing the file if permissions allow.
 *
 * If a file has been opened as multiple fids, possibly on different
 * connections, and one fid is used to remove the file, whether the
 * other fids continue to provide access to the file is
 * implementation-defined.  The Plan 9 file servers (like *fs(4)*)
 * remove the file immediately: attempts to use the other fids will
 * yield a ``phase error.''  *U9fs(4)* follows the semantics of the
 * underlying Unix file system, so other fids typically remain usable.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * **Remove** messages are generated by *remove*.
 *
 */

O(Tremove,	122,	T_REMOVE,	t_remove,	1, 0,	PIX_9P2000)
FS(t_remove)
	F(t_remove,	pixfid,	fid)
FE(t_remove)

O(Rremove,	123,	R_REMOVE,	r_remove,	0, 1,	PIX_9P2000)
FS(r_remove)
FE(r_remove)


/**
 * @page	9P
 * @section	p9-stat	stat, wstat: inquire or change file attributes
 *
 * Description
 * ------------------------
 *
 * The **stat** transaction inquires about the file identified by *fid*.
 * The reply will contain a machine-independent *directory entry*,
 * *stat*, laid out as follows:
 *
 * *size*[2]:
 * total byte count of the following data
 *
 * *type*[2]:
 * for kernel use
 *
 * *dev*[4]:
 * for kernel use
 *
 * *qid*.type[1]:
 * the type of the file (directory, etc.), represented as a bit vector
 * corresponding to the high 8 bits of the file's mode word.
 *
 * *qid*.vers[4]:
 * version number for given path
 *
 * *qid*.path[8]:
 * the file server's unique identification for the file
 *
 * *mode*[4]:
 * permissions and flags
 *
 * *atime*[4]:
 * last access time
 *
 * *mtime*[4]:
 * last modification time
 *
 * *length*[8]:
 * length of file in bytes
 *
 * *name*[s]:
 * file name; must be
 * **`/`**
 * if the file is the root directory of the server
 *
 * *uid*[s]:
 * owner name
 *
 * *gid*[s]:
 * group name
 *
 * *muid*[s]:
 * name of the user who last modified the file
 *
 * Integers in this encoding are in little-endian order (least
 * significant byte first).  The *convM2D* and *convD2M* routines (see
 * *fcall(2)*) convert between directory entries and a C structure
 * called a **Dir**.
 *
 * The *mode* contains permission bits as described in *intro(5)* and
 * the following:
 *
 *  + **0x80000000** (**DMDIR**, this file is a directory),
 *  + **0x40000000** (**DMAPPEND**, append only),
 *  + **0x20000000** (**DMEXCL**, exclusive use),
 *  + **0x04000000** (**DMTMP**, temporary);
 *
 * these are echoed in **Qid.type**.  Writes to append-only files always
 * place their data at the end of the file; the *offset* in the
 * **write** message is ignored, as is the **OTRUNC** bit in an open.
 * Exclusive use files may be open for I/O by only one fid at a time
 * across all clients of the server.  If a second open is attempted, it
 * draws an error.  Servers may implement a timeout on the lock on an
 * exclusive use file: if the fid holding the file open has been unused
 * for an extended period (of order at least minutes), it is reasonable
 * to break the lock and deny the initial fid further I/O.  Temporary
 * files are not included in nightly archives (see *fossil(4)*).
 *
 * The two time fields are measured in seconds since the epoch (Jan 1
 * 00:00 1970 GMT).  The *mtime* field reflects the time of the last
 * change of content (except when later changed by **wstat**.  For a
 * plain file, *mtime* is the time of the most recent **create**,
 * **open** with truncation, or **write**; for a directory it is the
 * time of the most recent **remove**, **create**, or **wstat** of a
 * file in the directory.  Similarly, the *atime* field records the last
 * **read** of the contents; also it is set whenever *mtime* is set.  In
 * addition, for a directory, it is set by an **attach**, **walk**, or
 * **create**, all whether successful or not.
 *
 * The *muid* field names the user whose actions most recently changed
 * the *mtime* of the file.
 *
 * The *length* records the number of bytes in the file.  Directories
 * and most files representing devices have a conventional length of 0.
 *
 * The **stat** request requires no special permissions.
 *
 * The **wstat** request can change some of the file status information.
 *
 * The *name* can be changed by anyone with write permission in the
 * parent directory; it is an error to change the name to that of an
 * existing file.
 *
 * The *length* can be changed (affecting the actual length of the file)
 * by anyone with write permission on the file.  It is an error to
 * attempt to set the length of a directory to a non-zero value, and
 * servers may decide to reject length changes for other reasons.
 *
 * The *mode* and *mtime* can be changed by the owner of the file or the
 * group leader of the file's current group.  The directory bit cannot
 * be changed by a **wstat**; the other defined permission and mode bits
 * can.
 *
 * The *gid* can be changed: by the owner if also a member of the new
 * group; or by the group leader of the file's current group if also
 * leader of the new group (see *intro(5)* for more information about
 * permissions and *users(6)* for users and groups).
 *
 * None of the other data can be altered by a **wstat** and attempts to
 * change them will trigger an error.  In particular, it is illegal to
 * attempt to change the owner of a file.  (These conditions may be
 * relaxed when establishing the initial state of a file server; see
 * *fsconfig(8)*.)
 *
 * Either all the changes in **wstat** request happen, or none of them
 * does: if the request succeeds, all changes were made; if it fails,
 * none were.
 *
 * A **wstat** request can avoid modifying some properties of the file
 * by providing explicit "don't touch" values in the **stat** data that
 * is sent: zero-length strings for text values and the maximum unsigned
 * value of appropriate size for integral values.
 *
 * As a special case, if *all* the elements of the directory entry in a
 * **Twstat** message are "don't touch" values, the server may interpret
 * it as a request to guarantee that the contents of the associated file
 * are committed to stable storage before the **Rwstat** message is
 * returned.  (Consider the message to mean, "make the state of the file
 * exactly what it claims to be.")
 *
 * A *read* of a directory yields an integral number of directory
 * entries in the machine independent encoding given above (see
 * *read(5)*).
 *
 * Note that since the **stat** information is sent as a 9P
 * variable-length datum, it is limited to a maximum of 65535 bytes.
 *
 *
 * Entry Points
 * ------------------------------------
 *
 * **Stat** messages are generated by *fstat* and *stat*.
 *
 * **Wstat** messages are generated by *fwstat* and *wstat*.
 *
 *
 * Bugs
 * ------------------------------------
 *
 * To make the contents of a directory, such as returned by *read(5)*,
 * easy to parse, each directory entry begins with a size field.  For
 * consistency, the entries in **Twstat** and **Rstat** messages also
 * contain their size, which means the size appears twice.  For example,
 * the **Rstat** message is formatted as
 *
 * (4+1+2+2+*n*)[4] **Rstat** *tag*[2] *n*[2] (*n*-2)[2] *type*[2] *dev*[4] ...
 *
 * where *n* is the value returned by **convD2M**.
 *
 */

O(Tstat,	124,	T_STAT,		t_stat,		1, 0,	PIX_9P2000)
FS(t_stat)
	F(t_stat,	pixfid,	fid)
FE(t_stat)

O(Rstat,	125,	R_STAT,		r_stat,		0, 1,	PIX_9P2000)
FS(r_stat)
	F(r_stat,	pix2,	nstat)
	F(r_stat,	pixdent, stat)
FE(r_stat)

O(Twstat,	126,	T_WSTAT,	t_wstat,	1, 0,	PIX_9P2000)
FS(t_wstat)
	F(t_wstat,	pixfid,	fid)
	F(t_wstat,	pix2,	nstat)
	F(t_wstat,	pixdent, stat)
FE(t_wstat)

O(Rwstat,	127,	R_WSTAT,	r_wstat,	0, 1,	PIX_9P2000)
FS(r_wstat)
FE(r_wstat)


/*
	// From `//linux/include/net/9p/9p.h' ---
	LT_LERROR      =  6,
	LR_LERROR      =  7,
	LT_STATFS      =  8,
	LR_STATFS      =  9,
	LT_LOPEN       = 12,
	LR_LOPEN       = 13,
	LT_LCREATE     = 14,
	LR_LCREATE     = 15,
	LT_SYMLINK     = 16,
	LR_SYMLINK     = 17,
	LT_MKNOD       = 18,
	LR_MKNOD       = 19,
	LT_RENAME      = 20,
	LR_RENAME      = 21,
	LT_READLINK    = 22,
	LR_READLINK    = 23,
	LT_GETATTR     = 24,
	LR_GETATTR     = 25,
	LT_SETATTR     = 26,
	LR_SETATTR     = 27,
	LT_XATTRWALK   = 30,
	LR_XATTRWALK   = 31,
	LT_XATTRCREATE = 32,
	LR_XATTRCREATE = 33,
	LT_READDIR     = 40,
	LR_READDIR     = 41,
	LT_FSYNC       = 50,
	LR_FSYNC       = 51,
	LT_LOCK        = 52,
	LR_LOCK        = 53,
	LT_GETLOCK     = 54,
	LR_GETLOCK     = 55,
	LT_LINK        = 70,
	LR_LINK        = 71,
	LT_MKDIR       = 72,
	LR_MKDIR       = 73,
	LT_RENAMEAT    = 74,
	LR_RENAMEAT    = 75,
	LT_UNLINKAT    = 76,
	LR_UNLINKAT    = 77,
*/


/*
   <https://wiki.qemu.org/Documentation/9p>

 * <http://ericvh.github.io/9p-rfc/rfc9p2000.u.html>

struct pix_9p2000u_r_error  { pixS ename; pix4 errnox; };
struct pix_9p2000u_t_create {
	pixfid fid; pixS name; pix4 perm; pix1 mode; pixS extension; };


 * <http://ericvh.github.io/9p-rfc/rfc9p2000.L.html>
 * <https://github.com/chaos/diod/blob/master/protocol.md>

struct pix_9p2000L_t_auth        {
	pixfid afid; pixS uname; pixS aname; pix4 n_uname; };
struct pix_9p2000L_t_attach      {
	pixfid fid; pixfid afid; pixS uname; pixS aname; pix4 n_uname; };
struct pix_9p2000L_r_lerror      {
	pix4 ecode; };
struct pix_9p2000L_t_statfs      { pixfid fid; };
struct pix_9p2000L_r_statfs      {
	pix4 type; pix4 bsize; pix8 blocks; pix8 bfree; pix8 bavail;
	pix8 files; pix8 ffree; pix8 fsid; pix4 namelen; };
struct pix_9p2000L_t_lopen       { pixfid fid; pix4 flags; };
struct pix_9p2000L_r_lopen       { pixqid qid; pix4 iounit; };
struct pix_9p2000L_t_lcreate     {
	pixfid fid; pixS name; pix4 flags; pix4 mode; pix4 gid; };
struct pix_9p2000L_r_lcreate     { pixqid qid; pix4 iounit; };
struct pix_9p2000L_t_symlink     {
	pixfid fid; pixS name; pixS symtgt; pix4 gid; };
struct pix_9p2000L_r_symlink     { pixqid qid; };
struct pix_9p2000L_t_mknod       {
	pixfid dfid; pixS name; pix4 mode;
	pix4 major; pix4 minor; pix4 gid; };
struct pix_9p2000L_r_mknod       { pixqid qid; };

	// [...]

struct pix_9p2000L_t_link        { pixfid dfid; pixfid fid; pixS name; };
struct pix_9p2000L_r_link        { };
struct pix_9p2000L_t_mkdir       {
	pixfid dfid; pixS name; pix4 mode; pix4 gid; };
struct pix_9p2000L_r_mkdir       { pixqid qid; };
struct pix_9p2000L_t_renameat    {
	pix4 olddirfd; pixS oldname; pix4 newdirfd; pixS newname; };
struct pix_9p2000L_r_renameat    { };
struct pix_9p2000L_t_unlinkat    { pix4 dirfd; pixS name; pix4 flags; };
struct pix_9p2000L_r_unlinkat    { };


<https://github.com/chaos/diod/blob/master/protocol.md>
Additionally, 9P2000.L defines

[...]

	size[4] Trename tag[2] fid[4] dfid[4] name[s]
	size[4] Rrename tag[2]

	size[4] Treadlink tag[2] fid[4]
	size[4] Rreadlink tag[2] target[s]


	size[4] Tgetattr tag[2] fid[4] request_mask[8]
	size[4] Rgetattr tag[2] valid[8] qid[13] mode[4] uid[4] gid[4] nlink[8]
					 rdev[8] size[8] blksize[8] blocks[8]
					 atime_sec[8] atime_nsec[8] mtime_sec[8] mtime_nsec[8]
					 ctime_sec[8] ctime_nsec[8] btime_sec[8] btime_nsec[8]
					 gen[8] data_version[8]

	size[4] Tsetattr tag[2] fid[4] valid[4] mode[4] uid[4] gid[4] size[8]
					 atime_sec[8] atime_nsec[8] mtime_sec[8] mtime_nsec[8]
	size[4] Rsetattr tag[2]

	size[4] Txattrwalk tag[2] fid[4] newfid[4] name[s]
	size[4] Rxattrwalk tag[2] size[8]

	size[4] Txattrcreate tag[2] fid[4] name[s] attr_size[8] flags[4]
	size[4] Rxattrcreate tag[2]

	size[4] Treaddir tag[2] fid[4] offset[8] count[4]
	size[4] Rreaddir tag[2] count[4] data[count]

		dents are variable-length records:
		qid[13] offset[8] type[1] name[s]


	size[4] Tfsync tag[2] fid[4]
	size[4] Rfsync tag[2]

	size[4] Tlock tag[2] fid[4] type[1] flags[4] start[8] length[8] proc_id[4] client_id[s]
	size[4] Rlock tag[2] status[1]

	size[4] Tgetlock tag[2] fid[4] type[1] start[8] length[8] proc_id[4] client_id[s]
	size[4] Rgetlock tag[2] type[1] start[8] length[8] proc_id[4] client_id[s]

[...]

*/

#undef O
#undef FS
#undef F
#undef FE
