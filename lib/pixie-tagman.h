/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  pixie-tagman.h
 * @brief 9P implementation: tag manager
 */

#include <stdlib.h>

#include "pixie.h"

////////////////////////////////////////////////////////////////////////

/**
 * Tag allocator: tracks tags in use.
 *
 * From man:intro(9p):
 * > Each **T**-message has a tag field, chosen and used by the client
 * > to identify the message.  The reply to the message will have the
 * > same tag.  Clients must arrange that no two outstanding messages on
 * > the same connection have the same tag.  An exception is the tag
 * > ``NOTAG``: the client can use it, when establishing a connection,
 * > to override tag matching in version messages.
 */
struct pix_tagman;

/** Set up a new tag manager. */
struct pix_tagman *pix_tagman_new (void);
/** Get the number of tags in use. */
size_t pix_tagman_size (struct pix_tagman *);
/** Get a new tag. */
pixtag pix_tagman_alloc (struct pix_tagman *);
/** Release a tag. */
void pix_tagman_free (struct pix_tagman *, pixtag);
/** Release a tag manager. */
void pix_tagman_drop (struct pix_tagman *);

////////////////////////////////////////////////////////////////////////
