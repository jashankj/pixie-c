/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-proto.c
 * @brief 9P implementation: protocol exchange
 */

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "pixie.h"
#include "pixie-cb.h"
#include "pixie-proto.h"
#include "pixie-raw.h"

////////////////////////////////////////////////////////////////////////

static int pcb_dispatch (struct pix_pcb *self, struct pix_raw *msg);

static inline pixV newestv (struct pix_pcb *self);
static inline bool IS_9P2000 (struct pix_pcb *self);
static inline bool IS_9P2000_u (struct pix_pcb *self);
static inline bool IS_9P2000_L (struct pix_pcb *self);

#define PIX_TYPE_MAX R_WSTAT
typedef CB_TYPE(pix_proto_cb) cbtype;

/*
 * Protocol control block.
 */
struct pix_pcb {
	unsigned int dialects;
	cbtype cb[PIX_TYPE_MAX];
};

////////////////////////////////////////////////////////////////////////

struct pix_pcb *
pix_proto_new (void)
{
	struct pix_pcb *new = malloc (sizeof *new);
	if (new == NULL) return new;

	new->dialects = PIX_9P2000;
	for (unsigned i = 0; i < PIX_TYPE_MAX; i++)
		new->cb[i] = CB_NULL_OF (cbtype);

	return new;
}

int
pix_proto_set_dialect (struct pix_pcb *self, unsigned mask)
{
	unsigned oldmask = self->dialects;
	self->dialects = mask;
	return (int) oldmask;
}

void
pix_proto_drop (struct pix_pcb *self)
{
	if (self == NULL) return;
	free (self);
}

////////////////////////////////////////////////////////////////////////

int
pix_proto_recv_bytes (
	struct pix_pcb *self,
	const uint8_t  *inbuf,
	const size_t    inlen
) {
	assert (self != NULL);
	assert (inbuf != NULL);
	assert (inlen > 0);

	/* Do we have enough data to make a parse? */
	if (! pix_raw_decode_enough (inbuf, inlen))
		return -1;

	/* We do!  Try to parse. */
	struct pix_raw msg;
	ptrdiff_t got =
		pix_raw_decode (inbuf, inlen, newestv (self), &msg);
	assert (got > 0); /* Notionally impossible: we have enough data! */

	return pix_proto_recv_msg (self, &msg);
}

int
pix_proto_recv_msg (
	struct pix_pcb *self,
	struct pix_raw *msg
) {
	assert (self != NULL);
	assert (msg != NULL);

	int err;
	if ((err = pcb_dispatch (self, msg)) < 0)
		return err - 1;

	return 0;
}


////////////////////////////////////////////////////////////////////////
// Callbacks.

int
pix_proto_register_cb (
	struct pix_pcb *self,
	enum pix_type   type,
	pix_proto_cb   *cbfunc,
	void           *cbdata
) {
	assert (self != NULL);
	assert (cbfunc != NULL);

	if (self->cb[type].cbfunc != NULL)
		return -1;

	self->cb[type] = CB_OF (cbtype, cbfunc, cbdata);
	return 0;
}

int
pix_proto_unregister_cb (
	struct pix_pcb *self,
	enum pix_type   type,
	pix_proto_cb   *cbfunc,
	void           *cbdata
) {
	assert (self != NULL);
	assert (cbfunc != NULL);

	if (
		self->cb[type].cbfunc == NULL ||
		self->cb[type].cbfunc != cbfunc ||
		self->cb[type].cbdata != cbdata
	)
		return -1;

	self->cb[type] = CB_NULL_OF (cbtype);
	return 0;
}

static int
pcb_dispatch (struct pix_pcb *self, struct pix_raw *msg)
{
	assert (self != NULL);
	assert (msg != NULL);

	return CB_INVOKE(&self->cb[msg->type], msg);
}

////////////////////////////////////////////////////////////////////////

static inline pixV
newestv (struct pix_pcb *self)
{
	if (IS_9P2000_L (self)) return PIX_9P2000_L;
	if (IS_9P2000_u (self)) return PIX_9P2000_u;
	if (IS_9P2000   (self)) return PIX_9P2000;
	__builtin_unreachable ();
}

static inline bool
IS_9P2000 (struct pix_pcb *self)
{
	return (self->dialects & PIX_9P2000) != 0;
}

static inline bool
IS_9P2000_u (struct pix_pcb *self)
{
	return (self->dialects & PIX_9P2000_u) != 0;
}

static inline bool
IS_9P2000_L (struct pix_pcb *self)
{
	return (self->dialects & PIX_9P2000_L) != 0;
}

////////////////////////////////////////////////////////////////////////
