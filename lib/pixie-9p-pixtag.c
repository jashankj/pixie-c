/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pixtag.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

pixN_cmp(tag)
/** Deserialize an `xtag'. */
DE0(pixtag)   { return de (pix2, buf, len, dialect, into); }
pixN_eq(tag)
FMT0(pixtag)  { return fmt (pix2, self); }
SER0(pixtag)  { return ser (pix2, from, into); }
SIZE_(pixtag) { return size (pix2); }

////////////////////////////////////////////////////////////////////////
