/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-attach.c
 * @brief 9P implementation: 9P2000 Tattach message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_attach)
{
	return
		size (pixfid) +
		size (pixfid) +
		size (pixS, &self->uname) +
		size (pixS, &self->aname);
}

SER(pix_9p_t_attach)
{
	SER_PRE (size (pix_9p_t_attach, from));

	ser (pix4, &from->fid,   into);
	ser (pix4, &from->afid,  into);
	ser (pixS, &from->uname, into);
	ser (pixS, &from->aname, into);

	return (ptrdiff_t) size (pix_9p_t_attach, from);
}

DE(pix_9p_t_attach)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_attach out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->fid);
	DO_PARSE (&data, total, pix4, &into->afid);
	DO_PARSE (&data, total, pixS, &into->uname);
	DO_PARSE (&data, total, pixS, &into->aname);

	if (into == &out) {
		free (into->uname.text);
		free (into->aname.text);
	}

	return total;
}

FMT(pix_9p_t_attach)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_attach);
	CHECK (writef ("Tattach"));
	CHECK (writef (" tag "));   CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid "));   CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" afid "));  CHECK (fmt (pixfid, &self->afid));
	CHECK (writef (" uname ")); CHECK (fmt (pixS,   &self->uname));
	CHECK (writef (" aname ")); CHECK (fmt (pixS,   &self->aname));
	return 0;
}

EQ(pix_9p_t_attach)
{
	return
		eq (a, b, pixfid, fid) &&
		eq (a, b, pixfid, afid) &&
		eq (a, b, pixS,   uname) &&
		eq (a, b, pixS,   aname);
}

////////////////////////////////////////////////////////////////////////
