/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pixS.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

CMP0(pixS)
{
	NULLS_CMP (a, b);
	assert (a != NULL && b != NULL);
	if (a->s != b->s) return (a->s - b->s);
	assert (a->s == b->s);
	return memcmp (a->text, b->text, a->s);
}

/** Deserialize a string. */
DE0(pixS)
{
	assert (buf != NULL);

	/* TODO(jashankj): rewrite this using the do-parse logic. */

	/* Read length (a leading `x2'). */
	ptrdiff_t ret;
	pix2 slen;
	if ((ret = de (pix2, buf, len, dialect, &slen)) <= 0) return ret;
	assert (ret > 0); assert (ret == 2);

	/* Bounds-check. */
	if (len-ret < slen) return (len-ret) - slen;

	if (into == NULL) goto done;
	assert (into != NULL);
	assert (len - ret >= slen);

	/* Prepare to copy up. */
	into->s    = slen;
	into->text = malloc (slen);
	if (into->text == NULL) return 0;

	for (int i = 0; i != slen; i++)
		into->text[i] = pix1_of_bytes (buf[ret + i]);

done:
	return ret + slen;
}

EQ0(pixS) { return pixS_cmp (a, b) == 0; }

FMT0(pixS) { return sbuf_bcpy(buf, self->text, self->s); }
FMT0(pixSQ)
{
	CHECK (writec ('\''));
	CHECK (fmt (pixS, self));
	CHECK (writec ('\''));
	return 0;
}

/** Serialise a `pixS'. */
SER0(pixS)
{
	SER_PRE (size (pixS, from));

	ser (pix2, &from->s, into);
	for (unsigned i = 0; i < from->s; i++)
		ser (pix1, &from->text[i], into);

	return (ptrdiff_t) size (pixS, from);
}

SIZE0(pixS)
{
	return
		size (pix2) +
		self->s;
}

////////////////////////////////////////////////////////////////////////

#include <zf_log.h>

static inline void *
memdup (const void *that, const size_t len)
{
	void *this = malloc (len);
	memcpy (this, that, len);
	return this;
}

typedef size_t (objlen_f) (const void *);

static inline pixS *
pixS_of (pixS *s, const void *obj, objlen_f *objlen)
{
	*s = (pixS){ .s = 0, .text = NULL };
	if (obj == NULL) return s;
	if ((s->s    = objlen (obj))       == 0) return s;
	if ((s->text = memdup (obj, s->s)) == NULL) {
		ZF_LOGE ("failed to clone string");
		return NULL;
	}
	assert (s->s > 0 && s->text != NULL);
	return s;
}

pixS *
pixS_of_cstr (pixS *s, const char *str)
{
	return pixS_of (s, str, (objlen_f *) strlen);
}

pixS *
pixS_of_wcstr (pixS *s, const wchar_t *str)
{
	return pixS_of (s, str, (objlen_f *) wcslen);
}

////////////////////////////////////////////////////////////////////////
