/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pix1.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

pixN_cmp(1)
/** Deserialize a `pix1'. */
pixN_de(1, (buf[0]))
pixN_eq(1)
FMT0(pix1) { return writef ("%" PRIu8, *self); }

/** Serialise a `pix1'. */
SER0(pix1)
{
	SER_PRE (size (pix1));

	u8 x0 = byte_0_of_pix1 (*from);
	buf_append (into, &x0, 1);

	return (ptrdiff_t) size (pix1);
}
SIZE_(pix1) { return 1; }

////////////////////////////////////////////////////////////////////////
