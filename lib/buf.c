/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2020-22, The University of New South Wales, Australia.
 */

/**
 * @file  src/buf.c
 * @brief a simple dynamic-sized buffer
 *
 * somewhat inspired by utstring, and rust's vec type.
 */

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "buf.h"

#define BUF_INITIAL_CAPACITY 8

static void buf_realloc (struct buf *self, size_t new_cap);


struct buf *
buf_new (void)
{
	return buf_new_with_size (sizeof (void *));
}

struct buf *
buf_init (struct buf *self)
{
	return buf_init_with_size (self, sizeof (void *));
}

struct buf *
buf_new_with_size (const size_t isize)
{
	return buf_new_with_size_with_cap (isize, BUF_INITIAL_CAPACITY);
}

struct buf *
buf_init_with_size (struct buf *self, const size_t isize)
{
	return buf_init_with_size_with_cap (
		self, isize, BUF_INITIAL_CAPACITY
	);
}

struct buf *
buf_new_with_size_with_cap (size_t isize, const size_t cap)
{
	struct buf *new = malloc (sizeof *new);
	if (new == NULL) return NULL;
	assert (new != NULL);
	return buf_init_with_size_with_cap (new, isize, cap);
}

struct buf *
buf_init_with_size_with_cap (
	struct buf   *self,
	const size_t  isize,
	const size_t  cap)
{
	assert (self != NULL);
	assert (cap > 0);
	*self = (struct buf) {
		.isize = isize,
		.cap   = 0,
		.len   = 0,
		.items = NULL
	};
	buf_realloc (self, cap);
	return self;
}

struct buf *
buf_init_from_raw_parts (
	struct buf *self,
	size_t      cap,
	size_t      len,
	size_t      isize,
	void       *items
) {
	(*self) = (struct buf) {
		.cap   = cap,
		.len   = len,
		.isize = isize,
		.items = items
	};
	return self;
}

struct buf *
buf_from_raw_parts (
	size_t  cap,
	size_t  len,
	size_t  isize,
	void   *items
) {
	struct buf *new = malloc (sizeof *new);
	if (new == NULL) return NULL;
	assert (new != NULL);
	return buf_init_from_raw_parts (new, cap, len, isize, items);
}

static void
buf_realloc (struct buf *self, const size_t new_cap)
{
	void *new_items =
		realloc (self->items, new_cap * self->isize);
	if (new_items == NULL) abort (); // XXX
	self->items = new_items;
	self->cap   = new_cap;
}

void
buf_append (struct buf *self, const void *data, const size_t len)
{
	assert (self != NULL);
	assert (data != NULL);

	// if we'd run out of room, expand.
	if (self->len + len > self->cap)
		buf_realloc (self, self->cap + len);

	uintptr_t at = buf_length_bytes (self);
	void *at_ptr = &((uint8_t *) self->items)[at];
	size_t n_data_bytes = len * self->isize;
	memcpy (at_ptr, data, n_data_bytes);
	self->len += len;
}

void
buf_drain (struct buf *self, struct buf *other)
{
	assert (self != NULL);
	assert (other != NULL);
	assert (self->isize == other->isize);

	buf_append (self, other->items, other->len);
	other->len = 0; // and wipe the other buffer.
}

void
buf_split_at (struct buf *self, size_t idx, struct buf **other)
{
	assert (self != NULL);
	assert (other != NULL);
	if (idx > self->len) return;

	if (*other == NULL)
		*other = buf_new_with_size_with_cap (
			self->isize, self->len - idx);
	assert (*other != NULL);

	uintptr_t at = idx * self->isize;
	void *at_ptr = &((uint8_t *) self->items)[at];
	buf_append (*other, at_ptr, self->len - idx);
	buf_realloc (self, self->len = idx);
}

void
buf_drop (struct buf *self)
{
	if (self == NULL) return;
	buf_drop_inner (self);
	free (self);
}

void
buf_drop_inner (struct buf *self)
{
	assert (self != NULL);
	free (self->items);
}

size_t
buf_length (struct buf *self)
{
	return self->len;
}

size_t
buf_length_bytes (struct buf *self)
{
	return self->len * self->isize;
}

size_t
buf_capacity (struct buf *self)
{
	return self->cap;
}

size_t
buf_capacity_bytes (struct buf *self)
{
	return self->cap * self->isize;
}

size_t
buf_item_size (struct buf *self)
{
	return self->isize;
}


void
buf_truncate (struct buf *self)
{
	self->len = 0;
}
