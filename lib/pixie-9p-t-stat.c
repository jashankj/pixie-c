/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-stat.c
 * @brief 9P implementation: 9P2000 Tstat message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_stat)
{
	return
		size (pixfid);
}

SER(pix_9p_t_stat)
{
	SER_PRE (size (pix_9p_t_stat, from));

	ser (pixfid, &from->fid, into);

	return (ptrdiff_t) size (pix_9p_t_stat, from);
}

DE(pix_9p_t_stat)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_stat out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixfid, &into->fid);

	return total;
}

FMT(pix_9p_t_stat)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_stat);
	CHECK (writef ("Tstat"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid ")); CHECK (fmt (pixfid, &self->fid));
	return 0;
}

EQ(pix_9p_t_stat)
{
	return
		eq (a, b, pixfid, fid);
}

////////////////////////////////////////////////////////////////////////
