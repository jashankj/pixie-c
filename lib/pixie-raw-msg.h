/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  pixie-raw-msg.h
 * @brief 9P implementation: macro hacks for defining a message
 *
 * This file is full of the very best C preprocessor evil the lint in my
 * pocket could buy.  These shenanigans are used to implement all the
 * stuff needed for each type of message.
 */

#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bsd/sys/cdefs.h>
#include <bsd/sys/sbuf.h>

#include "buf.h"

#include "pixie.h"
#include "pixie-raw.h"
#include "pixie-raw-gbpb.h"
#include "pixie-raw-prim.h"

#pragma clang diagnostic ignored "-Wunused-parameter"
#pragma clang diagnostic ignored "-Wunused-variable"
#pragma clang diagnostic ignored "-Wunused-function"

#ifndef u8
#define u8 uint8_t
#endif


////////////////////////////////////////////////////////////////////////
//// Helpful types and macros for calculating message sizes.

#define SIZE(tagname) \
	size_t tagname##_size ( \
		const struct tagname *self)
#define SIZE0(type) \
	size_t type##_size ( \
		const type *self)
#define SIZE_(type) \
	size_t type##_size (void)

#define size(ty,...) ty##_size(__VA_ARGS__)


////////////////////////////////////////////////////////////////////////
//// Helpful types/macros for deserialising/parsing.

/**
 * This is the general shape and behaviour of all the deserialisers.
 *
 * @param[in]	buf	input byte-stream
 * @param[in]	len	currently-available bytes
 * @param[in]	dialect	the dialect to parse the message in
 * @param[out]	into	location to read into;
 *     or NULL to check if enough bytes are available
 * @returns	0	if there is no match or an error occurred
 * @returns	>0	for the number of bytes matched
 * @returns	<0	for the number of bytes still required
 */
typedef ptrdiff_t (*de) (
	const u8 *buf, const size_t len,
	const unsigned dialect, void *into);

#define DE(tagname) \
	ptrdiff_t tagname##_de ( \
		const uint8_t  *buf, \
		const size_t    len, \
		const pixV      dialect, \
		struct tagname *into)
#define DE0(type) \
	ptrdiff_t type##_de ( \
		const uint8_t *buf, \
		const size_t   len, \
		const pixV     dialect, \
		type          *into)
#define de(ty,...) ty##_de(__VA_ARGS__)


typedef struct buffer { const u8 *buf; size_t len; } BUF;

static inline void
BUF_INCR (BUF *x, ptrdiff_t n)
{
	x->buf += n;
	x->len -= (size_t) n;
}

/**
 * Use `parser' to extract data from `data' and store the results into
 * `target' (and add the length to `total').
 */
#define DO_PARSE(data, total, type, target) \
	({ \
		ptrdiff_t ret = de ( \
			type, (data)->buf, (data)->len, dialect, (target)); \
		if (ret <= 0) return ret; \
		else BUF_INCR ((data), ret); \
		(total) += ret; \
	})

#define pixN_de(N, parse) \
	ptrdiff_t \
	pix##N##_de ( \
		const u8    *buf, \
		const size_t len, \
		const pixV   dialect, \
		pix##N      *into) \
	{ \
		assert (buf != NULL); \
		const size_t tylen = pix##N##_size (); \
		if (len < tylen) \
			return (ptrdiff_t)(len) - (ptrdiff_t)(tylen); \
		assert (len >= tylen); \
		if (into != NULL) \
			*into = pix##N##_of_bytes parse; \
		return (ptrdiff_t) tylen; \
	}

////////////////////////////////////////////////////////////////////////
//// Helpful types/macros for serialising/unparsing.

/**
 * This is the general shape and behaviour of all the serialisers.
 *
 * @param[in]	from	the object to serialise from
 * @param[in]	into	the location to output into.
 * @returns	0	if an error occurred
 * @returns	>0	for the number of bytes output
 */
typedef ptrdiff_t (*ser) (const void *from, struct buf *into);

#define SER(tagname) \
	ptrdiff_t tagname##_ser ( \
		const struct tagname *from, \
		struct buf           *into)
#define SER0(type) \
	ptrdiff_t type##_ser ( \
		const type *from, \
		struct buf *into)
#define ser(ty,...) ty##_ser(__VA_ARGS__)

/**
 * How much of a specific buffer is left?
 */
static inline size_t
buf_remaining (struct buf *into)
{
	return buf_capacity (into) - buf_length (into);
}

#define SER_PRE(size) \
	({ \
		assert (from != NULL); \
		assert (into != NULL); \
		assert (buf_remaining (into) >= (size)); \
	})

////////////////////////////////////////////////////////////////////////
//// Helpful types and macros for formatted output.

#define FMT(tagname) \
	int tagname##_fmt ( \
		const struct tagname *self, \
		struct sbuf          *buf)
#define FMT0(type) \
	int type##_fmt ( \
		const type  *self, \
		struct sbuf *buf)
#define fmt(ty, self) ty##_fmt(self, buf)

#define CHECK(x) \
	({ int e; if ((e = (x))) { return e; } })

#define writec(c)       sbuf_putc (buf, c)
#define writef(fmt,...) sbuf_printf (buf, fmt, ##__VA_ARGS__)

#define DUMPL 64

static int dumpsome (struct sbuf *buf, pix4 count, pix1 *data);
static inline bool is_printable (pix1 ch) __pure2;

/**
 * Dump out count (or DUMPL, if count is bigger) bytes from data to buf,
 * as a string if they are all printable, else as a series of hex bytes.
 */
static int
dumpsome (struct sbuf *buf, pix4 count, pix1 *data)
{
	if (data == NULL)
		return writef ("<no data>");

	bool printable = true;
	if (count > DUMPL)
		count = DUMPL;
	for (unsigned i = 0; i < count; i++)
		if (! is_printable (data[i]))
			printable = false;

	CHECK (writec('\''));

	if (printable) {
		for (unsigned i = 0; i < count; i++)
			CHECK (writec (data[i]));

	} else {
		for (unsigned i = 0; i < count; i++) {
			if (i > 0 && i % 4 == 0)
				CHECK (writec (' '));
			CHECK (writef ("%2x", data[i]));
		}
	}

	CHECK (writec('\''));
	return 0;
}

static inline bool
is_printable (pix1 ch)
{
	return (32 <= ch && ch <= 127) || ch == '\n' || ch == '\t';
}

typedef pixS pixSQ;
int pixSQ_fmt (const pixSQ *self, struct sbuf *buf);


////////////////////////////////////////////////////////////////////////
//// Helpful types/macros for comparison and equality.

#define CMP(tagname) \
	int tagname##_cmp ( \
		const struct tagname *a, \
		const struct tagname *b)
#define CMP0(type) \
	int type##_cmp ( \
		const type *a, \
		const type *b)
#define cmp(a, b, T, f) \
	(T##_cmp (&((a)->f), &((b)->f)))
#define cmpeq(a, b, T, f) \
	(cmp(a, b, T, f) == 0)

#define NULLS_CMP(a,b) \
	({ \
		if (a == b) return 0; \
		if (a == NULL && b != NULL) return  1; \
		if (a != NULL && b == NULL) return -1; \
	})

#define pixN_cmp(N) \
	int \
	pix##N##_cmp ( \
		const pix##N *a, \
		const pix##N *b) \
	{ \
		NULLS_CMP (a, b); \
		assert (a != NULL && b != NULL); \
		return (int)((intptr_t)*a - (intptr_t)*b); \
	}

#define EQ(tagname) \
	bool tagname##_eq ( \
		const struct tagname *a, \
		const struct tagname *b)
#define EQ0(type) \
	bool type##_eq ( \
		const type *a, \
		const type *b)
#define eq(a, b, T, f) \
	(T##_eq (&((a)->f), &((b)->f)))

#define pix_raw_eq_common( \
	/* (const struct pix_raw *) */ a, \
	/* (const struct pix_raw *) */ b \
) ( \
	eq (a, b, pix4, size) && \
	eq (a, b, pix1, type) && \
	eq (a, b, pixtag, tag) \
)

#define NULLS_EQ(a,b) \
	({ \
		if (a == b) return true; \
		if (a == NULL || b == NULL) return false; \
	})

#define pixN_eq(N) \
	bool \
	pix##N##_eq ( \
		const pix##N *a, \
		const pix##N *b) \
	{ \
		NULLS_EQ (a, b); \
		return pix##N##_cmp (a, b) == 0; \
	}


////////////////////////////////////////////////////////////////////////
//// Miscellanea.

int pix1_of_pixtype (pixV dialect, pixtype type) __pure;
int pixtype_of_pix1 (pixV dialect, pix1 byte) __pure;
bool pixtype_request_p (pixtype type) __pure;
bool pixtype_response_p (pixtype type) __pure;
pixtype pixtype_response_to (pixV dialect, pixtype reqty) __pure;
int pixtype_request_of (pixV dialect, pixtype resty) __pure;


////////////////////////////////////////////////////////////////////////
