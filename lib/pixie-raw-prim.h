/*-
 * SPDX-License-Identifier: BSD-2-Clause AND MIT
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 *
 * Portions derived from the Plan 9 from Bell Labs distribution,
 * which is MIT licensed by the Plan 9 Foundation.
 * Contains code from
 */

#pragma once

/**
 * @file  pixie-raw-prim.h
 * @brief 9P implementation: primitives for the raw protocol
 */

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <bsd/sys/cdefs.h>
#include <bsd/sys/sbuf.h>

#include "buf.h"
#include "pixie.h"
#include "pixie-raw.h"

////////////////////////////////////////////////////////////////////////

int  pixtag_cmp (const pixtag  *a, const pixtag  *b);
int  pixfid_cmp (const pixfid  *a, const pixfid  *b);
int  pixS_cmp   (const pixS    *a, const pixS    *b);
int  pix1_cmp   (const pix1    *a, const pix1    *b);
int  pix2_cmp   (const pix2    *a, const pix2    *b);
int  pix4_cmp   (const pix4    *a, const pix4    *b);
int  pix8_cmp   (const pix8    *a, const pix8    *b);

////////////////////////////////////////////////////////////////////////

ptrdiff_t pix_raw_decode (
	const uint8_t     *buf,
	const size_t       buflen,
	const pixV         dialect,
	struct pix_raw *msg);
bool pix_raw_decode_enough (const uint8_t *buf, size_t buflen);

/* public: */
ptrdiff_t pix_raw_de (
	const uint8_t     *buf,
	const size_t       len,
	const pixV         dialect,
	struct pix_raw *into);

/* tu-only: */
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	ptrdiff_t \
	pix_9p_##x_xxx##_de ( \
		const uint8_t *buf, \
		const size_t   len, \
		const pixV     dialect, \
		struct pix_9p_##x_xxx *into);
#include "pixie-msgtypes.inc"
#undef O

/* private: */
#define O(x) \
	ptrdiff_t \
	x##_de ( \
		const uint8_t *buf, \
		const size_t   len, \
		const pixV     dialect, \
		x             *into);
#include "pixie-primtypes.inc"
#undef O

////////////////////////////////////////////////////////////////////////

/* public: */
bool pix_raw_eq (
	const struct pix_raw *a,
	const struct pix_raw *b);

/* tu-only: */
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	bool \
	pix_9p_##x_xxx##_eq ( \
		const struct pix_9p_##x_xxx *a, \
		const struct pix_9p_##x_xxx *b);
#include "pixie-msgtypes.inc"
#undef O

/* private: */
#define O(x) \
	bool \
	x##_eq (\
		const x *a, \
		const x *b);
#include "pixie-primtypes.inc"
#undef O

////////////////////////////////////////////////////////////////////////

/* public: */
int pix_raw_fmt (const struct pix_raw *self, struct sbuf *buf);

/* tu-only: */
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	int \
	pix_9p_##x_xxx##_fmt ( \
		const struct pix_9p_##x_xxx *self, \
		struct sbuf *buf);
#include "pixie-msgtypes.inc"
#undef O

/* private: */
#define O(x) \
	int \
	x##_fmt  ( \
		const x     *self, \
		struct sbuf *into);
#include "pixie-primtypes.inc"
#undef O

////////////////////////////////////////////////////////////////////////

ptrdiff_t pix_raw_encode (
	const struct pix_raw *msg, uint8_t **buf, size_t *buflen);

/* public: */
ptrdiff_t pix_raw_ser (const struct pix_raw *from, struct buf *into);

/* tu-only: */
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	ptrdiff_t \
	pix_9p_##x_xxx##_ser ( \
		const struct pix_9p_##x_xxx *from, \
		struct buf              *into);
#include "pixie-msgtypes.inc"
#undef O

/* private: */
#define O(x) \
	ptrdiff_t \
	x##_ser  ( \
		const x    *from, \
		struct buf *into);
#include "pixie-primtypes.inc"
#undef O

////////////////////////////////////////////////////////////////////////

/* public: */
size_t pix_raw_size (const struct pix_raw *self);

/* tu-only: */
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	size_t \
	pix_9p_##x_xxx##_size ( \
		const struct pix_9p_##x_xxx *self);
#include "pixie-msgtypes.inc"
#undef O

/* private: */
size_t pixtype_size (void) __pure;
size_t pixtag_size  (void) __pure;
size_t pixfid_size  (void) __pure;
size_t pixqid_size  (void) __pure;
size_t pixdent_size (const pixdent *self);
size_t pixS_size    (const pixS *self);
size_t pix1_size    (void) __pure;
size_t pix2_size    (void) __pure;
size_t pix4_size    (void) __pure;
size_t pix8_size    (void) __pure;

////////////////////////////////////////////////////////////////////////
