/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2020-22, The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  src/buf.h
 * @brief a simple dynamic-sized buffer
 *
 * somewhat inspired by utstring, and rust's vec type.
 */

#include <bsd/sys/cdefs.h>

#include <stddef.h>
#include <stdlib.h>

__BEGIN_DECLS


/** Simple growable buffer type. */
struct buf {
	size_t cap;   /**< capacity, in items. { 0 < cap }. */
	size_t len;   /**< length, in items. { 0 <= len < cap }. */
	size_t isize; /**< size of each item, in bytes. */
	void *_Nullable items; /**< the items proper. */
};

/** Allocate a new buffer, storing pointer-sized items. */
struct buf *_Nullable buf_new (void)
	__exported __malloc_like __result_use_check;
/** allocate a new buffer, storing `isize'-sized items. */
struct buf *_Nullable buf_new_with_size (const size_t isize)
	__exported __malloc_like __result_use_check;
/** Allocate a new buffer, storing `isize'-sized items,
 * with a specific capacity. */
struct buf *_Nullable buf_new_with_size_with_cap (
	const size_t isize, const size_t cap)
	__exported __malloc_like __result_use_check;

/** Initialise a new buffer in-place, storing pointer-sized items. */
struct buf *_Nonnull buf_init (struct buf *_Nonnull self)
	__exported;
/** Initialise a new buffer in-place, storing `isize'-sized items. */
struct buf *_Nonnull buf_init_with_size (
	struct buf *_Nonnull self,
	const size_t         isize
) __exported;
/** Initialise a new buffer in-place, storing `isize'-sized items,
 * with a specific capacity. */
struct buf *_Nonnull buf_init_with_size_with_cap (
	struct buf *_Nonnull self,
	const size_t         isize,
	const size_t         cap
) __exported;

/** Turn an existing allocated object into a `buf'. [UNSAFE] */
struct buf *_Nonnull buf_init_from_raw_parts (
	struct buf *_Nonnull self,
	size_t               cap,
	size_t               len,
	size_t               isize,
	void *_Nullable      items
) __exported;

/** Turn an existing allocated object into a `buf'. [UNSAFE] */
struct buf *_Nonnull buf_from_raw_parts (
	size_t          cap,
	size_t          len,
	size_t          isize,
	void *_Nullable items
) __exported __malloc_like __result_use_check;

/** To buffer `self', append `len' items from `data'. */
void buf_append (
	struct buf *_Nonnull self,
	const void *_Nonnull data,
	const size_t         len
) __exported;
/** To buffer `self', append every item in `other'. */
void buf_drain (
	struct buf *_Nonnull self,
	struct buf *_Nonnull other
) __exported;
/** At index `idx', split buffer `self'; all the remaining items move to
 * newly-initialised (and optionally newly-allocated) `other'. */
void buf_split_at (
	struct buf *_Nonnull            self,
	const size_t                    idx,
	struct buf *_Nullable *_Nonnull other
) __exported;

/** Deallocate buffer `self'. */
void buf_drop (struct buf *_Nonnull self)
	__exported;
/** Deinitialise buffer `self'. */
void buf_drop_inner (struct buf *_Nonnull self)
	__exported;

/** Get the number of items in buffer `self'. */
size_t buf_length (struct buf *_Nonnull self)
	__exported;
/** Get the number of bytes in buffer `self'. */
size_t buf_length_bytes (struct buf *_Nonnull self)
	__exported;

/** Get the capacity in items, in buffer `self'. */
size_t buf_capacity (struct buf *_Nonnull self)
	__exported;
/** Get the capacity in bytes, in buffer `self'. */
size_t buf_capacity_bytes (struct buf *_Nonnull self)
	__exported;

/** Get the size of the items in buffer `self'. */
size_t buf_item_size (struct buf *_Nonnull self)
	__exported;

void buf_truncate (struct buf *_Nonnull self)
	__exported;

#ifdef ZF_LOGD
#define buf_debug( \
	/* (const char *) */       name, \
	/* (const struct buf *) */ self \
) ({ \
	ZF_LOGD ( \
		"%s: %p = {cap=%zu, len=%zu, isize=%zu, items=%p}", \
		(name), \
		(const void *) (self), \
		(self)->cap, \
		(self)->len, \
		(self)->isize, \
		(self)->items \
	); \
})
#endif

__END_DECLS
