/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-attach.c
 * @brief 9P implementation: 9P2000 Rattach message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_attach)
{
	return
		size (pixqid);
}

SER(pix_9p_r_attach)
{
	SER_PRE (size (pix_9p_r_attach, from));

	ser (pixqid, &from->qid, into);

	return (ptrdiff_t) size (pix_9p_r_attach, from);
}

DE(pix_9p_r_attach)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_attach out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixqid, &into->qid);

	return total;
}

FMT(pix_9p_r_attach)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_attach);
	CHECK (writef ("Rattach"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" qid ")); CHECK (fmt (pixqid, &self->qid));
	return 0;
}

EQ(pix_9p_r_attach)
{
	return
		eq (a, b, pixqid, qid);
}

////////////////////////////////////////////////////////////////////////
