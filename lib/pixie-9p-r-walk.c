/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-walk.c
 * @brief 9P implementation: 9P2000 Rwalk message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_walk)
{
	return
		size (pix2) +
		(self->nwqid * size (pixqid));
}

SER(pix_9p_r_walk)
{
	SER_PRE (size (pix_9p_r_walk, from));

	ser (pix2, &from->nwqid, into);
	for (unsigned i = 0; i < from->nwqid; i++)
		ser (pixqid, &from->wqid[i], into);

	return (ptrdiff_t) size (pix_9p_r_walk, from);
}

DE(pix_9p_r_walk)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_walk out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix2, &into->nwqid);
	into->wqid = calloc (into->nwqid, sizeof (pixqid));
	if (into->wqid == NULL) return 0;
	for (unsigned i = 0; i < into->nwqid; i++)
		DO_PARSE (&data, total, pixqid, &into->wqid[i]);

	if (into == &out) {
		free (into->wqid);
	}

	return total;
}

FMT(pix_9p_r_walk)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_walk);
	CHECK (writef ("Rwalk"));
	CHECK (writef (" tag "));   CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" nwqid ")); CHECK (fmt (pix2,   &self->nwqid));
	if (self->nwqid <= PIX_MAXWELEM)
		for (int i = 0; i < self->nwqid; i++) {
			CHECK (writef ("%d:", i));
			CHECK (fmt (pixqid, &self->wqid[i]));
			CHECK (writec (' '));
		}
	return 0;
}

EQ(pix_9p_r_walk)
{
	if (! eq (a, b, pix2, nwqid)) return false;
	for (unsigned i = 0; i < a->nwqid; i++)
		if (! eq (a, b, pixqid, wqid[i]))
			return false;
	return true;
}

////////////////////////////////////////////////////////////////////////
