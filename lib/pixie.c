/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie.c
 * @brief yet another 9P2000 client/server library, in C
 *
 * ... but with some really weird constraints.
 * For example, no I/O: users provide us buffers of bytes.
 * For example, it's callbacks all the way down.
 */

#include <assert.h>

#include "pixie.h"
