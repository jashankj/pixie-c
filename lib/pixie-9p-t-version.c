/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-version.c
 * @brief 9P implementation: 9P2000 Tversion message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_version)
{
	return
		size (pix4) +
		size (pixS, &self->version);
}

SER(pix_9p_t_version)
{
	SER_PRE (size (pix_9p_t_version, from));

	ser (pix4, &from->msize,   into);
	ser (pixS, &from->version, into);

	return (ptrdiff_t) size (pix_9p_t_version, from);
}

DE(pix_9p_t_version)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_version out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->msize);
	DO_PARSE (&data, total, pixS, &into->version);

	if (into == &out) {
		free (into->version.text);
	}

	return total;
}

FMT(pix_9p_t_version)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_version);
	CHECK (writef ("Tversion"));
	CHECK (writef (" tag "));     CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" msize "));   CHECK (fmt (pix4,   &self->msize));
	CHECK (writef (" version ")); CHECK (fmt (pixS,   &self->version));
	return 0;
}

EQ(pix_9p_t_version)
{
	return
		eq (a, b, pix4, msize) &&
		eq (a, b, pixS, version);
}

////////////////////////////////////////////////////////////////////////
