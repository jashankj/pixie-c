/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-open.c
 * @brief 9P implementation: 9P2000 Ropen message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_open)
{
	return
		size (pixqid) +
		size (pix1);
}

SER(pix_9p_r_open)
{
	SER_PRE (size (pix_9p_r_open, from));

	ser (pixqid, &from->qid,    into);
	ser (pix4,   &from->iounit, into);

	return (ptrdiff_t) size (pix_9p_r_open, from);
}

DE(pix_9p_r_open)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_open out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixqid, &into->qid);
	DO_PARSE (&data, total, pix4,   &into->iounit);

	return total;
}

FMT(pix_9p_r_open)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_open);
	CHECK (writef ("Ropen"));
	CHECK (writef (" tag "));    CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" qid "));    CHECK (fmt (pixqid, &self->qid));
	CHECK (writef (" iounit ")); CHECK (fmt (pix4,   &self->iounit));
	return 0;
}

EQ(pix_9p_r_open)
{
	return
		eq (a, b, pixqid, qid) &&
		eq (a, b, pix4,   iounit);
}

////////////////////////////////////////////////////////////////////////
