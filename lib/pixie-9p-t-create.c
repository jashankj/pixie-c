/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-create.c
 * @brief 9P implementation: 9P2000 Tcreate message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_create)
{
	return
		size (pixfid) +
		size (pixS, &self->name) +
		size (pix4) +
		size (pix1);
}

SER(pix_9p_t_create)
{
	SER_PRE (size (pix_9p_t_create, from));

	ser (pix4, &from->fid,  into);
	ser (pixS, &from->name, into);
	ser (pix4, &from->perm, into);
	ser (pix1, &from->mode, into);

	return (ptrdiff_t) size (pix_9p_t_create, from);
}

DE(pix_9p_t_create)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_create out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->fid);
	DO_PARSE (&data, total, pixS, &into->name);
	DO_PARSE (&data, total, pix4, &into->perm);
	DO_PARSE (&data, total, pix1, &into->mode);

	return total;
}

FMT(pix_9p_t_create)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_create);
	CHECK (writef ("Tcreate"));
	CHECK (writef (" tag "));  CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid "));  CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" name ")); CHECK (fmt (pixS,   &self->name));
	CHECK (writef (" perm ")); CHECK (fmt (pix4,   &self->perm));
	CHECK (writef (" mode ")); CHECK (fmt (pix1,   &self->mode));
	return 0;
}

EQ(pix_9p_t_create)
{
	return
		eq (a, b, pixfid, fid) &&
		eq (a, b, pixS,   name) &&
		eq (a, b, pix4,   perm) &&
		eq (a, b, pix1,   mode);
}

////////////////////////////////////////////////////////////////////////
