/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-open.c
 * @brief 9P implementation: 9P2000 Topen message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_open)
{
	return
		size (pixfid) +
		size (pix1);
}

SER(pix_9p_t_open)
{
	SER_PRE (size (pix_9p_t_open, from));

	ser (pixfid, &from->fid,  into);
	ser (pix1,   &from->mode, into);

	return (ptrdiff_t) size (pix_9p_t_open, from);
}

DE(pix_9p_t_open)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_open out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixfid, &into->fid);
	DO_PARSE (&data, total, pix1,   &into->mode);

	return total;
}

FMT(pix_9p_t_open)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_open);
	CHECK (writef ("Topen"));
	CHECK (writef (" tag "));  CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid "));  CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" mode ")); CHECK (fmt (pix1,   &self->mode));
	return 0;
}

EQ(pix_9p_t_open)
{
	return
		eq (a, b, pixfid, fid) &&
		eq (a, b, pix1,   mode);
}

////////////////////////////////////////////////////////////////////////
