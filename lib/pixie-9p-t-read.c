/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-read.c
 * @brief 9P implementation: 9P2000 Tread message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_read)
{
	return
		size (pixfid) +
		size (pix8) +
		size (pix4);
}

SER(pix_9p_t_read)
{
	SER_PRE (size (pix_9p_t_read, from));

	ser (pix4, &from->fid,    into);
	ser (pix8, &from->offset, into);
	ser (pix4, &from->count,  into);

	return (ptrdiff_t) size (pix_9p_t_read, from);
}

DE(pix_9p_t_read)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_read out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->fid);
	DO_PARSE (&data, total, pix8, &into->offset);
	DO_PARSE (&data, total, pix4, &into->count);

	return total;
}

FMT(pix_9p_t_read)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_read);
	CHECK (writef ("Tread"));
	CHECK (writef (" tag "));    CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid "));    CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" offset ")); CHECK (fmt (pix8,   &self->offset));
	CHECK (writef (" count "));  CHECK (fmt (pix4,   &self->count));
	return 0;
}

EQ(pix_9p_t_read)
{
	return
		eq (a, b, pixfid, fid) &&
		eq (a, b, pix8,   offset) &&
		eq (a, b, pix4,   count);
}

////////////////////////////////////////////////////////////////////////
