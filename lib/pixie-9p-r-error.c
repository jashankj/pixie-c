/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-error.c
 * @brief 9P implementation: 9P2000 Rerror message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_error)
{
	return
		size (pixS, &self->ename);
}

SER(pix_9p_r_error)
{
	SER_PRE (size (pix_9p_r_error, from));

	ser (pixS, &from->ename, into);

	return (ptrdiff_t) size (pix_9p_r_error, from);
}

DE(pix_9p_r_error)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_error out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixS, &into->ename);

	if (into == &out) {
		free (into->ename.text);
	}

	return total;
}

FMT(pix_9p_r_error)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_error);
	CHECK (writef ("Rerror"));
	CHECK (writef (" tag "));   CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" ename ")); CHECK (fmt (pixS,   &self->ename));
	return 0;
}

EQ(pix_9p_r_error)
{
	return
		eq (a, b, pixS, ename);
}

////////////////////////////////////////////////////////////////////////
