/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-raw.c
 * @brief 9P implementation: encoding/... entry points
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

ptrdiff_t
pix_raw_decode (
	const u8       *buf,
	const size_t    len,
	const unsigned  dialect,
	struct pix_raw *into)
{
	return pix_raw_de (buf, len, dialect, into);
}

bool
pix_raw_decode_enough (const u8 *buf, const size_t len)
{
	assert (buf != NULL);

	pix4 msglen;
	if (pix4_de (buf, len, PIX_9P2000, &msglen) != 4) return false;

	return len >= msglen;
}

/** Encode a raw 9P message. */
ptrdiff_t
pix_raw_encode (
	const struct pix_raw *msg,
	uint8_t             **buf,
	size_t               *buflen
) {
	struct buf out;
	buf_init_with_size_with_cap (&out, 1, size (pix_raw, msg));
	ptrdiff_t ret = ser (pix_raw, msg, &out);
	*buf    = out.items;
	*buflen = out.len;
	return ret;
}

////////////////////////////////////////////////////////////////////////

DE(pix_raw)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_raw out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->size);

	/* use advertised message length to judge. */
	if (len < into->size) return len - into->size;

	DO_PARSE (&data, total, pixtype, &into->type);
	DO_PARSE (&data, total, pixtag,  &into->tag);

	switch (into->type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		DO_PARSE (&data, total, pix_9p_##x_xxx, &into->x_xxx); \
		break;
#include "pixie-msgtypes.inc"
#undef O
	}
	return total;
}

EQ(pix_raw)
{
	NULLS_EQ (a, b);
	if (! eq (a, b, pix4,    size)) return false;
	if (! eq (a, b, pixtype, type)) return false;
	if (! eq (a, b, pixtag,  tag))  return false;

	switch (a->type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		return pix_9p_##x_xxx##_eq (&a->x_xxx, &b->x_xxx);
#include "pixie-msgtypes.inc"
#undef O
	}
}

FMT(pix_raw)
{
	assert (self != NULL);
	assert (buf != NULL);

	switch (self->type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		return pix_9p_##x_xxx##_fmt (&self->x_xxx, buf);
#include "pixie-msgtypes.inc"
#undef O
	}
}

SER(pix_raw)
{
	SER_PRE (size (pix_raw, from));
	size_t olen = buf_length (into);

	ser (pix4,    &from->size, into);
	ser (pixtype, &from->type, into);
	ser (pixtag,  &from->tag,  into);

	switch (from->type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		pix_9p_##x_xxx##_ser (&from->x_xxx, into); \
		break;
#include "pixie-msgtypes.inc"
#undef O
	}
	return buf_length (into) - olen;
}

static size_t pix_raw_size_inner (const struct pix_raw *self);

SIZE(pix_raw)
{
	return
		/* Common 9P message header (size + type + tag). */
		size (pix4) +
		size (pixtype) +
		size (pixtag) +
		pix_raw_size_inner (self);
}

/** Switched size for each variant. */
static size_t
pix_raw_size_inner (const struct pix_raw *self)
{
	assert (self != NULL);

	switch (self->type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		return pix_9p_##x_xxx##_size (&self->x_xxx);
#include "pixie-msgtypes.inc"
#undef O
	}
}

////////////////////////////////////////////////////////////////////////

bool
pix_raw_request_p (const struct pix_raw *self)
{
	assert (self != NULL);
	return pixtype_request_p (self->type);
}

bool
pix_raw_response_p (const struct pix_raw *self)
{
	assert (self != NULL);
	return pixtype_response_p (self->type);
}

bool
pix_raw_reply_p (
	const struct pix_raw *request,
	const struct pix_raw *response
) {
	return
		request != NULL &&
		response != NULL &&
		pix_raw_request_p (request) &&
		pix_raw_response_p (response) &&
		request->tag == response->tag &&
		((
			request->type ==
				(pixtype) pixtype_request_of (
					(~0), response->type) &&
			response->type ==
				(pixtype) pixtype_response_to (
					(~0), request->type)
			) ||
			response->type == R_ERROR
		);
}


////////////////////////////////////////////////////////////////////////
