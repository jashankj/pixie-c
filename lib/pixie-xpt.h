/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2022 The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  pixie-xpt.h
 * @brief 9P transport layer definitions
 */

#include <bsd/sys/cdefs.h>

#include "pixie.h"

////////////////////////////////////////////////////////////////////////

enum pix_network { PIX_TCP, PIX_UDP, PIX_UNIX, PIX_ANY };
struct pix_dial {
	enum pix_network network;
	char *_Nonnull   netaddr;
	char *_Nullable  service;
};


/**
 * Parse a Plan 9-style dial string.
 *
 * Plan 9's `dial` system call takes a structured string argument, given
 * the name `addr`, but sometimes referred to as a *dial string*.
 *
 * From man:dial(2p):
 * > `addr` is a network address of the form `network!netaddr!service`,
 * > `network!netaddr`, or simply `netaddr`.
 * >
 * > `network` is `tcp`, `udp`, `unix`, or the special token, `net`:
 * > *net* is a free variable that stands for any network in common
 * > between the source and the host `netaddr`.
 * >
 * > `netaddr` can be a host name, a domain name, or a network address.
 *
 * In loose pseudo-ABNF::
 *
 *     dialstr  =  network  '!'  netaddr  '!'  service
 *              /  network  '!'  netaddr
 *              /                netaddr
 *
 * @param	dialstr	the "dial string" described above
 * @param	dial	a parsed dial string.
 * @returns 0 on success
 */
int pix_dialparse (const char *dialstr, struct pix_dial *dial);

////////////////////////////////////////////////////////////////////////
