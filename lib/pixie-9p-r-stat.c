/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-stat.c
 * @brief 9P implementation: 9P2000 Rstat message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_stat)
{
	return
		size (pix2) +
		size (pixdent, &self->stat);
}

SER(pix_9p_r_stat)
{
	SER_PRE (size (pix_9p_r_stat, from));

	ser (pix2,    &from->nstat, into);
	ser (pixdent, &from->stat,  into);

	return (ptrdiff_t) size (pix_9p_r_stat, from);
}

DE(pix_9p_r_stat)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_stat out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix2,    &into->nstat);
	DO_PARSE (&data, total, pixdent, &into->stat);

	if (into == &out) {
		free (into->stat.name.text);
		free (into->stat.uid.text);
		free (into->stat.gid.text);
		free (into->stat.muid.text);
	}

	return total;
}

FMT(pix_9p_r_stat)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_stat);
	CHECK (writef ("Rstat"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" stat"));
	if (self->nstat > 200) {
		CHECK (writec ('('));
		CHECK (fmt (pix2, &self->nstat));
		CHECK (writef (" bytes)"));
	} else {
		CHECK (fmt (pixdent, &self->stat));
	}
	return 0;
}

EQ(pix_9p_r_stat)
{
	return
		eq (a, b, pix2,    nstat) &&
		eq (a, b, pixdent, stat);
}

////////////////////////////////////////////////////////////////////////
