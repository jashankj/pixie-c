/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-write.c
 * @brief 9P implementation: 9P2000 Rwrite message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_write)
{
	return
		size (pix4);
}

SER(pix_9p_r_write)
{
	SER_PRE (size (pix_9p_r_write, from));

	ser (pix4, &from->count, into);

	return (ptrdiff_t) size (pix_9p_r_write, from);
}

DE(pix_9p_r_write)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_write out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->count);

	return total;
}

FMT(pix_9p_r_write)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_write);
	CHECK (writef ("Rwrite"));
	CHECK (writef (" tag "));   CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" count ")); CHECK (fmt (pix4,   &self->count));
	return 0;
}

EQ(pix_9p_r_write)
{
	return
		eq (a, b, pix4, count);
}

////////////////////////////////////////////////////////////////////////
