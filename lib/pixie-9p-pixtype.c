/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pixtype.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

int
pix1_of_pixtype (pixV dialect, pixtype type)
{
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	if ((dialects & dialect) && type == X_XXX) \
		return (int) N;
#include "pixie-msgtypes.inc"
#undef O

	return -1;
}

int
pixtype_of_pix1 (pixV dialect, pix1 byte)
{
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	if ((dialects & dialect) && byte == N) \
		return (int) X_XXX;
#include "pixie-msgtypes.inc"
#undef O

	return -1;
}

bool
pixtype_request_p (pixtype type)
{
	switch (type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		return (reqp) == 1;
#include "pixie-msgtypes.inc"
#undef O
	}
	__builtin_unreachable ();
}

bool
pixtype_response_p (pixtype type)
{
	switch (type) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		return (resp) == 1;
#include "pixie-msgtypes.inc"
#undef O
	}
	__builtin_unreachable ();
}

pixtype
pixtype_response_to (pixV dialect, pixtype reqty)
{
	assert (pixtype_request_p (reqty));
	pix1 byte = pix1_of_pixtype (dialect, reqty);
	byte += 1;
	return pixtype_of_pix1 (dialect, byte);
}

int
pixtype_request_of (pixV dialect, pixtype resty)
{
	assert (pixtype_response_p (resty));
	if (resty == R_ERROR) return -1;
	pix1 byte = pix1_of_pixtype (dialect, resty);
	byte -= 1;
	return (int) pixtype_of_pix1 (dialect, byte);
}

DE0(pixtype)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	enum pix_type out;
	if (into == NULL) into = &out;

	pix1 byte;
	DO_PARSE (&data, total, pix1, &byte);

	int ty;
	if ((ty = pixtype_of_pix1 (dialect, byte)) == -1)
		return -1;
	*into = (pixtype) ty;

	return total;
}

EQ0(pixtype)
{
	NULLS_EQ (a, b);
	return *a == *b;
}

FMT0(pixtype)
{
	switch (*self) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: \
		CHECK (writef (#Xxxx)); \
		break;
#include "pixie-msgtypes.inc"
#undef O
	}
	return 0;
}

SER0(pixtype)
{
	SER_PRE (size (pixtype));

	pix1 byte;
	switch (*from) {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	case X_XXX: byte = N; break;
#include "pixie-msgtypes.inc"
#undef O
	}

	return ser (pix1, &byte, into);
}

SIZE_(pixtype) { return size (pix1); }

////////////////////////////////////////////////////////////////////////
