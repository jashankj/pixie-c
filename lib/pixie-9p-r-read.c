/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-read.c
 * @brief 9P implementation: 9P2000 Rread message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_read)
{
	return
		size (pix4) +
		self->count;
}

SER(pix_9p_r_read)
{
	SER_PRE (size (pix_9p_r_read, from));

	ser (pix4, &from->count, into);
	// TODO(jashankj): load data

	return (ptrdiff_t) size (pix_9p_r_read, from);
}

DE(pix_9p_r_read)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_read out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->count);
	// TODO(jashankj): load data

	return total;
}

FMT(pix_9p_r_read)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_read);
	CHECK (writef ("Rread"));
	CHECK (writef (" tag "));   CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" count ")); CHECK (fmt (pix4,   &self->count));
	CHECK (writec (' '));
	return dumpsome (buf, self->count, self->data);
}

EQ(pix_9p_r_read)
{
	return
		eq (a, b, pix4, count) &&
		memcmp (a->data, b->data, a->count) == 0;
}

////////////////////////////////////////////////////////////////////////
