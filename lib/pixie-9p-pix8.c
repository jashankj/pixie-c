/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pix8.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

pixN_cmp(8)
/** Deserialize a `pix8'. */
pixN_de(8, (buf[0], buf[1], buf[2], buf[3], buf[4], buf[5], buf[6], buf[7]))
pixN_eq(8)
FMT0(pix8) { return writef ("%" PRIu64, *self); }

/** Serialise a `pix8'. */
SER0(pix8)
{
	SER_PRE (size (pix8));

	u8 x0 = byte_0_of_pix8 (*from);
	buf_append (into, &x0, 1);
	u8 x1 = byte_1_of_pix8 (*from);
	buf_append (into, &x1, 1);
	u8 x2 = byte_2_of_pix8 (*from);
	buf_append (into, &x2, 1);
	u8 x3 = byte_3_of_pix8 (*from);
	buf_append (into, &x3, 1);
	u8 x4 = byte_4_of_pix8 (*from);
	buf_append (into, &x4, 1);
	u8 x5 = byte_5_of_pix8 (*from);
	buf_append (into, &x5, 1);
	u8 x6 = byte_6_of_pix8 (*from);
	buf_append (into, &x6, 1);
	u8 x7 = byte_7_of_pix8 (*from);
	buf_append (into, &x7, 1);

	return (ptrdiff_t) size (pix8);
}

SIZE_(pix8) { return 8; }

////////////////////////////////////////////////////////////////////////
