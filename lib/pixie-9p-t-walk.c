/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-walk.c
 * @brief 9P implementation: 9P2000 Twalk message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_walk)
{
	size_t ret = size (pixfid) + size (pixfid) + size (pix2);
	for (unsigned i = 0; i < self->nwname; i++)
		ret += size (pixS, &self->wname[i]);
	return ret;
}

SER(pix_9p_t_walk)
{
	SER_PRE (size (pix_9p_t_walk, from));

	ser (pixfid, &from->fid,    into);
	ser (pixfid, &from->newfid, into);
	ser (pix2,   &from->nwname, into);
	for (unsigned i = 0; i < from->nwname; i++)
		ser (pixS, &from->wname[i], into);

	return (ptrdiff_t) size (pix_9p_t_walk, from);
}

DE(pix_9p_t_walk)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_walk out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixfid, &into->fid);
	DO_PARSE (&data, total, pixfid, &into->newfid);
	DO_PARSE (&data, total, pix2,   &into->nwname);
	into->wname = calloc (into->nwname, sizeof (pixS));
	if (into->wname == NULL) return 0;
	for (unsigned i = 0; i < into->nwname; i++)
		DO_PARSE (&data, total, pixS, &into->wname[i]);

	if (into == &out) {
		for (unsigned i = 0; i < into->nwname; i++)
			free (into->wname[i].text);
		free (into->wname);
	}

	return total;
}

FMT(pix_9p_t_walk)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_walk);
	CHECK (writef ("Twalk"));
	CHECK (writef (" tag "));    CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid "));    CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" newfid ")); CHECK (fmt (pixfid, &self->newfid));
	CHECK (writef (" nwname ")); CHECK (fmt (pix2,   &self->nwname));
	if (self->nwname <= PIX_MAXWELEM)
		for (unsigned i = 0; i < self->nwname; i++) {
			CHECK (writef ("%d:", i));
			CHECK (fmt (pixS, &self->wname[i]));
			CHECK (writec (' '));
		}
	return 0;
}

EQ(pix_9p_t_walk)
{
	if (! eq (a, b, pixfid, fid))    return false;
	if (! eq (a, b, pixfid, newfid)) return false;
	if (! eq (a, b, pix2,   nwname)) return false;
	for (unsigned i = 0; i < a->nwname; i++)
		if (! eq (a, b, pixS, wname[i]))
			return false;
	return true;
}

////////////////////////////////////////////////////////////////////////
