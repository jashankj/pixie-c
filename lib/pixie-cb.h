/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2022 The University of New South Wales, Australia.
 */

#pragma once

#define CB_TYPE(cb_func_type) \
	struct { cb_func_type *cbfunc; void *cbdata; }

#define CB_OF(ty, func, data) \
	((ty){ .cbfunc = (func), .cbdata = (data) })

#define CB_NULL_OF(ty) \
	CB_OF(ty, NULL, NULL)

#define CB_INVOKE(cb, ...) \
	( \
		((cb) != NULL && ((cb)->cbfunc != NULL)) \
		? ((cb)->cbfunc)((cb)->cbdata, ##__VA_ARGS__) \
		: 0 \
	)

#define CB_INVOKE2(self, cb, ...) \
	( \
		((cb) != NULL && ((cb)->cbfunc != NULL)) \
		? ((cb)->cbfunc)((self), (cb)->cbdata, ##__VA_ARGS__) \
		: 0 \
	)


