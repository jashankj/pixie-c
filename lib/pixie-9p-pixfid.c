/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pixfid.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

pixN_cmp(fid)
/** Deserialize an `xfid'. */
DE0(pixfid)   { return de (pix4, buf, len, dialect, into); }
pixN_eq(fid)
FMT0(pixfid)  { CHECK (writef ("fid:")); return fmt (pix4, self); }
SER0(pixfid)  { return ser (pix4, from, into); }
SIZE_(pixfid) { return size (pix4); }

////////////////////////////////////////////////////////////////////////
