/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2022 The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  pixie-client.h
 * @brief 9P client implementation
 *
 * Smells strongly of lib9pclient from plan9port.
 */

#include "pixie.h"
#include "pixie-proto.h"
#include "pixie-xpt.h"

////////////////////////////////////////////////////////////////////////

typedef struct pix_client pixc;
typedef struct pixc_file  pixFid; /**< Rough analogue of `FILE *`. */
typedef struct pixc_dir   pixDir; /**< Rough analogue of `DIR *`. */

pixc *pix_client_new (void);
pixc *pix_client_new_from_pcb (struct pix_pcb *pcb);
void pix_client_drop (pixc *self);

int pixc_dial (pixc *self, const char *dialstr);
int pixc_hangup (pixc *self);

////////////////////////////////////////////////////////////////////////
/**
 * @defgroup	client	9P Client interface
 *
 * Users will likely want these functions as well as one of either the
 * synchronous (@ref client-sync) or asynchronous (@ref client-async)
 * interfaces.
 *
 * @defgroup	client-async	Asynchronous client interface
 *
 * @defgroup	client-sync	Synchronous, blocking client interface
 *
 * These probably won't ever get implemented, but probably should be!
 * Their primary value presently is to give an idea of what the async
 * interface should look like before building that.
 */

/**
 * @ingroup	client-sync
 * @brief	Execute an **auth** transaction.  (See @ref p9-attach.)
 * @param	self	current client
 * @param	uname	user name
 * @param	aname	attach name
 * @return	the auth Fid, to be used either in performing any
 *	further authentication, or to be passed to attach.
 */
pixFid *pixc_auth (
	pixc       *self,
	const char *uname,
	const char *aname);

typedef int pixc_auth_f (
	void   *cbdata,
	pixc   *cl,
	pixFid *afid);
/** @ingroup	client-async */
pixtag pixc_auth_async (
	pixc        *self,
	const char  *uname,
	const char  *aname,
	pixc_auth_f *cbfunc,
	void        *cbdata);


/**
 * @ingroup	client-sync
 * @brief	Execute an **attach** transaction.  (See @ref p9-attach.)
 *
 * Connects to the root of a file tree served by the server, presenting
 * the auth Fid to establish an authenticated identity.
 *
 * @param	self	current client
 * @param	afid	auth Fid
 * @param	uname	user name
 * @param	aname	attach name
 * @return	the Fid of the root of the remote filesystem
 */
pixFid *pixc_attach (
	pixc       *self,
	pixFid     *afid,
	const char *uname,
	const char *aname);

typedef int pixc_attach_f (
	void *cbdata, pixc *cl, pixFid *fid);
/** @ingroup	client-async */
int pixc_attach_async (
	pixc *self,
	const pixFid *afid,
	const char *uname,
	const char *aname,
	pixc_attach_f *cbfunc, void *cbdata);

/**
 * @ingroup	client
 */
pixFid *pixc_getroot (pixc *self);

/**
 * @ingroup	client
 */
void pixc_setroot (pixc *self,
	pixFid *fid);

/**
 * @ingroup	client
 */
void pixc_close (pixc *self,
	pixFid *fid);

/**
 * @ingroup	client-sync
 */
pixFid *pixc_create (pixc *self,
	const char *path,
	const int mode,
	const pix4 perm);

/**
 * @ingroup	client-sync
 */
int pixc_fcreate (pixc *self,
	pixFid *fid,
	const int mode, const pix4 perm);

/**
 * @ingroup	client-sync
 */
int pixc_remove (pixc *self,
	const char *path);

/**
 * @ingroup	client-sync
 */
int pixc_fremove (pixc *self, pixFid *fid);

/**
 * @ingroup	client-sync
 */
int pixc_access (pixc *self, const char *path, int amode);

/**
 * @ingroup	client-sync
 */
int pixc_fopen (pixc *self, pixFid *fid, int mode);

/**
 * @ingroup	client-sync
 */
int pixc_open (pixc *self, const char *path, int mode);

/**
 * @ingroup	client-sync
 */
long pixc_pread (pixc *self,
	pixFid *fid, void *buf, size_t len, off_t off);

/**
 * @ingroup	client-sync
 */
long pixc_pwrite (pixc *self,
	pixFid *fid, const void *buf, size_t len, off_t off);

/**
 * @ingroup	client-sync
 */
long pixc_read (pixc *self,
	pixFid *fid, void *buf, size_t len);

/**
 * @ingroup	client-sync
 */
long pixc_readn (pixc *self,
	pixFid *fid, void *buf, size_t len);

/**
 * @ingroup	client-sync
 */
long pixc_write (pixc *self,
	pixFid *fid, const void *buf, size_t len);

/**
 * @ingroup	client-sync
 */
long pixc_seek (pixc *self, pixFid *fid, off_t n, int type);

/**
 * @ingroup	client-sync
 */
pixqid pixc_qid (pixc *self, pixFid *fid);

/**
 * @ingroup	client-sync
 */
long pixc_dirread (pixc *self, pixFid *fid, pixDir **dir);

/**
 * @ingroup	client-sync
 */
long pixc_dirreadall (pixc *self, pixFid *fid, pixDir **dir);

/**
 * @ingroup	client-sync
 */
pixDir *pixc_dirfstat (pixc *self, pixFid *fid);

/**
 * @ingroup	client-sync
 */
pixDir *pixc_dirstat (pixc *self, char *path);

/**
 * @ingroup	client-sync
 */
int pixc_dirwstat (pixc *self, char *path, pixDir *d);

/**
 * @ingroup	client-sync
 */
int pixc_dirfwstat (pixc *self, pixFid *fid, pixDir *d);

////////////////////////////////////////////////////////////////////////
