/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-wstat.c
 * @brief 9P implementation: 9P2000 Twstat message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_wstat)
{
	return
		size (pixfid) +
		size (pix2) +
		size (pixdent, &self->stat);
}

SER(pix_9p_t_wstat)
{
	SER_PRE (size (pix_9p_t_wstat, from));

	ser (pixfid,  &from->fid,   into);
	ser (pix2,    &from->nstat, into);
	ser (pixdent, &from->stat,  into);

	return (ptrdiff_t) size (pix_9p_t_wstat, from);
}

DE(pix_9p_t_wstat)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_wstat out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixfid,  &into->fid);
	DO_PARSE (&data, total, pix2,    &into->nstat);
	DO_PARSE (&data, total, pixdent, &into->stat);

	if (into == &out) {
		free (into->stat.name.text);
		free (into->stat.uid.text);
		free (into->stat.gid.text);
		free (into->stat.muid.text);
	}

	return total;
}

FMT(pix_9p_t_wstat)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_wstat);
	CHECK (writef ("Twstat"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid ")); CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" stat"));
	if (self->nstat > 200) {
		/* no, I really don't know either.*/
		CHECK (writec ('('));
		CHECK (fmt (pix2, &self->nstat));
		CHECK (writef (" bytes)"));
	} else {
		CHECK (fmt (pixdent, &self->stat));
	}
	return 0;
}

EQ(pix_9p_t_wstat)
{
	return
		eq (a, b, pixfid,  fid) &&
		eq (a, b, pix2,    nstat) &&
		eq (a, b, pixdent, stat);
}

////////////////////////////////////////////////////////////////////////
