/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2022 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-primtypes.inc
 * @brief 9P implementation: table of message types
 */

#ifndef O
#error pixie-primtypes.inc included without "O" defined!
#endif

O(pixtype)
O(pixtag)
O(pixfid)
O(pixqid)
O(pixdent)
O(pixS)
O(pix1)
O(pix2)
O(pix4)
O(pix8)
