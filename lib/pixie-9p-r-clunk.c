/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-clunk.c
 * @brief 9P implementation: 9P2000 Rclunk message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_clunk)
{
	return 0;
}

SER(pix_9p_r_clunk)
{
	SER_PRE (size (pix_9p_r_clunk, from));

	return (ptrdiff_t) size (pix_9p_r_clunk, from);
}

DE(pix_9p_r_clunk)
{
	return 0;
}

FMT(pix_9p_r_clunk)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_clunk);
	CHECK (writef ("Rclunk"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	return 0;
}

EQ(pix_9p_r_clunk)
{
	return true;
}

////////////////////////////////////////////////////////////////////////
