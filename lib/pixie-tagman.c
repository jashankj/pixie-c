/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-tagman.c
 * @brief 9P implementation: tag manager
 */

#include <assert.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <bsd/sys/cdefs.h>
#include <bsd/sys/_types.h>
#include <bsd/sys/tree.h>

#include "pixie.h"
#include "pixie-tagman.h"

static const size_t MAX_N = (sizeof(pixtag) * 8) - 1;

struct tag {
	RB_ENTRY(tag) tree;
	pixtag tag;
};

struct pix_tagman {
	/** Most recently issued tag. */
	pixtag last;
	/** Number of tags in the tree. */
	size_t n;
	/** Tags in use. */
	RB_HEAD(tagt, tag) head;
};

static inline int
tagcmp (struct tag *e1, struct tag *e2)
{
	assert (e1 != NULL && e2 != NULL);
	return e1->tag < e2->tag ? -1 : e1->tag > e2->tag;
}

RB_PROTOTYPE(tagt, tag, tree, tagcmp)
RB_GENERATE(tagt, tag, tree, tagcmp)

struct pix_tagman *
pix_tagman_new (void)
{
	struct pix_tagman *new = malloc (sizeof *new);
	if (new == NULL) return NULL;
	new->last = NOTAG;
	new->n = 0;
	RB_INIT(&new->head);
	return new;
}

size_t
pix_tagman_size (struct pix_tagman *self)
{
	assert (self != NULL);
	return self->n;
}

/**
 * Given a particular tag, return the successor.
 * No, this isn't just ``this + 1`` --- we avoid returning NOTAG.
 */
static pixtag
pix_tag_succ (pixtag this)
{
	if (this     == NOTAG) return 0;
	if (this + 1 == NOTAG) return 0;
	return this + 1;
}

static struct tag *
pix_tag_by_tag (struct pix_tagman *self, pixtag that)
{
	struct tag find = { .tag = that };
	return RB_FIND (tagt, &self->head, &find);
}

pixtag
pix_tagman_alloc (struct pix_tagman *self)
{
	assert (self != NULL);
	assert (self->n < MAX_N);

	struct tag *new = malloc (sizeof *new);
	if (new == NULL) return NOTAG;
	assert (new != NULL);

	do
		new->tag = pix_tag_succ (self->last);
	while (RB_INSERT (tagt, &self->head, new) != NULL);

	self->n++;
	return self->last = new->tag;
}

void
pix_tagman_free (struct pix_tagman *self, pixtag tag)
{
	assert (self != NULL);
	assert (self->n != 0);
	assert (tag != NOTAG);
	struct tag *that = pix_tag_by_tag (self, tag);
	assert (that != NULL);
	struct tag *alsothat = RB_REMOVE (tagt, &self->head, that);
	assert (alsothat != NULL);
	self->n--;
	free (that);
	return;
}

void
pix_tagman_drop (struct pix_tagman *self)
{
	assert (self != NULL);

	struct tag *curr = RB_MIN (tagt, &self->head);
	while (curr != NULL) {
		struct tag *next = RB_NEXT (tagt, &self->head, curr);
		RB_REMOVE (tagt, &self->head, curr);
		free (curr);
		self->n--;
		curr = next;
	}

	assert (self->n == 0);
	free (self);
}
