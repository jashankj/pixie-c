/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-flush.c
 * @brief 9P implementation: 9P2000 Tflush message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_flush)
{
	return
		size (pixtag);
}

SER(pix_9p_t_flush)
{
	SER_PRE (size (pix_9p_t_flush, from));

	ser (pixtag, &from->oldtag, into);

	return (ptrdiff_t) size (pix_9p_t_flush, from);
}

DE(pix_9p_t_flush)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_flush out;
	DO_PARSE (&data, total, pixtag, &into->oldtag);

	return total;
}

FMT(pix_9p_t_flush)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_flush);
	CHECK (writef ("Tflush"));
	CHECK (writef (" tag "));    CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" oldtag ")); CHECK (fmt (pixtag, &self->oldtag));
	return 0;
}

EQ(pix_9p_t_flush)
{
	return
		eq (a, b, pixtag, oldtag);
}

////////////////////////////////////////////////////////////////////////
