/*-
 * SPDX-License-Identifier: BSD-2-Clause AND MIT
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 *
 * Portions derived from the Plan 9 from Bell Labs distribution,
 * which is MIT licensed by the Plan 9 Foundation.
 * Contains code from
 *   + //plan9/sys/src/libc/9sys/fcallfmt.c
 *   + //plan9/sys/include/fcall.h
 *   + //plan9/sys/include/libc.h
 */

#pragma once

/**
 * @file  pixie-raw.h
 * @brief 9P implementation: raw protocol
 */

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <bsd/sys/cdefs.h>
#include <bsd/sys/sbuf.h>

#include "pixie.h"

/** Message types. */
typedef enum pix_type {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	X_XXX,
#include "pixie-msgtypes.inc"
} pixtype;

/*
 * Convenience type-aliases to ensure the `fty' position is always a
 * single valid C identifier: an `A' suffix indicates an array.
 */
typedef pix1   *pix1A;
typedef pixS   *pixSA;
typedef pixqid *pixqidA;

#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
	struct pix_9p_##x_xxx
#define FS(x_xxx) \
	{
#define F(x_xxx, fty, fname) \
		fty fname;
#define FE(x_xxx) \
	};
#include "pixie-msgtypes.inc"


/**
 * A raw representation of a 9P request/reply.
 *
 * This is *not* a wire-level representation, but can readily be
 * constructed from one, or converted into one.
 *
 * Use `pix_raw_decode' to decode a request/reply from wire format.
 * Use `pix_raw_encode' to encode a request/reply to wire format.
 */
struct pix_raw {
	pix4    size;
	pixtype type;
	pixtag  tag;
	union {
#define O(Xxxx, N, X_XXX, x_xxx, reqp, resp, dialects) \
		struct pix_9p_##x_xxx	x_xxx;
#include "pixie-msgtypes.inc"
	};
};


////////////////////////////////////////////////////////////////////////
// Raw messages:

/**
 * Format a string representing a raw 9P message.
 */
int pix_raw_fmt (
	const struct pix_raw *self,
	struct sbuf          *buf);

/**
 * Decode a raw 9P message.
 */
ptrdiff_t pix_raw_decode (
	const uint8_t  *buf,
	const size_t    buflen,
	const pixV      dialect,
	struct pix_raw *msg);

/**
 * Have we got enough bytes in this buffer to decode a protocol message?
 * We assume the leading size bytes are accurate; and hope to receive
 * that many.  Otherwise, fail: we can't get a single whole message.
 */
bool pix_raw_decode_enough (
	const uint8_t *buf,
	const size_t   buflen);

/**
 * Encode a raw 9P message.
 */
ptrdiff_t pix_raw_encode (
	const struct pix_raw *self, uint8_t **buf, size_t *buflen);

/**
 * Get the size of a raw 9P message.
 */
size_t pix_raw_size (const struct pix_raw *self);

/** Is this a request? */
bool pix_raw_request_p (const struct pix_raw *self);

/** Is this a response? */
bool pix_raw_response_p (const struct pix_raw *self);

/** Are these messages a request and response pair? */
bool pix_raw_reply_p (
	const struct pix_raw *request,
	const struct pix_raw *response);

////////////////////////////////////////////////////////////////////////
