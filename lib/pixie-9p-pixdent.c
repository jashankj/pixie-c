/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pixdent.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

/** Deserialize an `xdent'. */
DE0(pixdent)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	pixdent out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix2,   &into->size);
	DO_PARSE (&data, total, pix2,   &into->type);
	DO_PARSE (&data, total, pix4,   &into->dev);
	DO_PARSE (&data, total, pixqid, &into->qid);
	DO_PARSE (&data, total, pix4,   &into->mode);
	DO_PARSE (&data, total, pix4,   &into->atime);
	DO_PARSE (&data, total, pix4,   &into->mtime);
	DO_PARSE (&data, total, pix8,   &into->length);
	DO_PARSE (&data, total, pixS,   &into->name);
	DO_PARSE (&data, total, pixS,   &into->uid);
	DO_PARSE (&data, total, pixS,   &into->gid);
	DO_PARSE (&data, total, pixS,   &into->muid);

	if (into == &out) {
		free (into->name.text);
		free (into->uid.text);
		free (into->gid.text);
		free (into->muid.text);
	}

	return total;
}

EQ0(pixdent)
{
	NULLS_EQ (a, b);
	assert (a != NULL && b != NULL);
	return
		eq (a, b, pix2,   size) &&
		eq (a, b, pix2,   type) &&
		eq (a, b, pix4,   dev) &&
		eq (a, b, pixqid, qid) &&
		eq (a, b, pix4,   mode) &&
		eq (a, b, pix4,   atime) &&
		eq (a, b, pix4,   mtime) &&
		eq (a, b, pix8,   length) &&
		eq (a, b, pixS,   name) &&
		eq (a, b, pixS,   uid) &&
		eq (a, b, pixS,   gid) &&
		eq (a, b, pixS,   muid);
}

FMT0(pixdent)
{
	CHECK (fmt (pixSQ, &self->name)); CHECK (writec (' '));
	CHECK (fmt (pixSQ, &self->uid));  CHECK (writec (' '));
	CHECK (fmt (pixSQ, &self->gid));  CHECK (writec (' '));
	CHECK (fmt (pixSQ, &self->muid));
	CHECK (writef (" q "));  CHECK (fmt (pixqid, &self->qid));
	CHECK (writef (" m "));  CHECK (fmt (pix4,   &self->mode));
	CHECK (writef (" at ")); CHECK (fmt (pix4,   &self->atime));
	CHECK (writef (" mt ")); CHECK (fmt (pix4,   &self->mtime));
	CHECK (writef (" l "));  CHECK (fmt (pix8,   &self->length));
	CHECK (writef (" t "));  CHECK (fmt (pix2,   &self->type));
	CHECK (writef (" d "));  CHECK (fmt (pix4,   &self->dev));
	return 0;
}

SER0(pixdent)
{
	SER_PRE (size (pixdent, from));

	ser (pix2,   &from->size,   into);
	ser (pix2,   &from->type,   into);
	ser (pix4,   &from->dev,    into);
	ser (pixqid, &from->qid,    into);
	ser (pix4,   &from->mode,   into);
	ser (pix4,   &from->atime,  into);
	ser (pix4,   &from->mtime,  into);
	ser (pix8,   &from->length, into);
	ser (pixS,   &from->name,   into);
	ser (pixS,   &from->uid,    into);
	ser (pixS,   &from->gid,    into);
	ser (pixS,   &from->muid,   into);

	return (ptrdiff_t) size (pixdent, from);
}

SIZE0(pixdent)
{
	return
		size (pix2) +
		size (pix2) +
		size (pix4) +
		size (pixqid) +
		size (pix4) +
		size (pix4) +
		size (pix4) +
		size (pix8) +
		size (pixS, &self->name) +
		size (pixS, &self->uid) +
		size (pixS, &self->gid) +
		size (pixS, &self->muid);
}

////////////////////////////////////////////////////////////////////////
