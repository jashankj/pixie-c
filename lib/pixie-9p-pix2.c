/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pix2.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

pixN_cmp(2)
/** Deserialize a `pix2'. */
pixN_de(2, (buf[0], buf[1]))
pixN_eq(2)
FMT0(pix2) { return writef ("%" PRIu16, *self); }

/** Serialise a `pix2'. */
SER0(pix2)
{
	SER_PRE (size (pix2));

	u8 x0 = byte_0_of_pix2 (*from);
	buf_append (into, &x0, 1);
	u8 x1 = byte_1_of_pix2 (*from);
	buf_append (into, &x1, 1);

	return (ptrdiff_t) size (pix2);
}

SIZE_(pix2) { return 2; }

////////////////////////////////////////////////////////////////////////
