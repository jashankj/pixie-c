/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pixqid.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

/** Deserialize a `pixqid'. */
DE0(pixqid)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	pixqid out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix1, &into->qtype);
	DO_PARSE (&data, total, pix4, &into->qver);
	DO_PARSE (&data, total, pix8, &into->qpath);

	return total;
}

EQ0(pixqid)
{
	NULLS_EQ (a, b);
	assert (a != NULL && b != NULL);
	return
		eq (a, b, pix1, qtype) &&
		eq (a, b, pix4, qver) &&
		eq (a, b, pix8, qpath);
}

FMT0(pixqid)
{
	// QIDFMT ~> "(%.16llux %lud %s)", of .path .vers .type
	CHECK (writec ('('));
	CHECK (fmt (pix8, &self->qpath));
	CHECK (writec (' '));
	CHECK (fmt (pix4, &self->qver));
	CHECK (writec (' '));
	pix1 t = self->qtype;
	if (t & PIX_QTDIR)    CHECK (writec ('d'));
	if (t & PIX_QTAPPEND) CHECK (writec ('a'));
	if (t & PIX_QTEXCL)   CHECK (writec ('l'));
	if (t & PIX_QTAUTH)   CHECK (writec ('A'));
	CHECK (writec (')'));
	return 0;
}

SER0(pixqid)
{
	SER_PRE (size (pixqid));

	ser (pix1, &from->qtype, into);
	ser (pix4, &from->qver,  into);
	ser (pix8, &from->qpath, into);

	return (ptrdiff_t) size (pixqid);
}

SIZE_(pixqid)
{
	return
		size (pix1) +
		size (pix4) +
		size (pix8);
}

////////////////////////////////////////////////////////////////////////
