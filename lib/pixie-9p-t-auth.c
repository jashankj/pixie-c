/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-auth.c
 * @brief 9P implementation: 9P2000 Tauth message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_auth)
{
	return
		size (pixfid) +
		size (pixS, &self->uname) +
		size (pixS, &self->aname);
}

SER(pix_9p_t_auth)
{
	SER_PRE (size (pix_9p_t_auth, from));

	ser (pix4, &from->afid,  into);
	ser (pixS, &from->uname, into);
	ser (pixS, &from->aname, into);

	return (ptrdiff_t) size (pix_9p_t_auth, from);
}

DE(pix_9p_t_auth)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_auth out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->afid);
	DO_PARSE (&data, total, pixS, &into->uname);
	DO_PARSE (&data, total, pixS, &into->aname);

	if (into == &out) {
		free (into->uname.text);
		free (into->aname.text);
	}

	return total;
}

FMT(pix_9p_t_auth)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_auth);
	CHECK (writef ("Tauth"));
	CHECK (writef (" tag "));   CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" afid "));  CHECK (fmt (pix4,   &self->afid));
	CHECK (writef (" uname ")); CHECK (fmt (pixS,   &self->uname));
	CHECK (writef (" aname ")); CHECK (fmt (pixS,   &self->aname));
	return 0;
}

EQ(pix_9p_t_auth)
{
	return
		eq (a, b, pix4, afid) &&
		eq (a, b, pixS, uname) &&
		eq (a, b, pixS, aname);
}

////////////////////////////////////////////////////////////////////////
