/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2020-22, The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  pixie-raw-gbpb.h
 * @brief 9P implementation: code for {G,P}BIT{8,16,32,64}.
 *
 * but somehow simultaneously more and less terrifying.
 */

#include <bsd/sys/cdefs.h>
#include <stdint.h>

#include "pixie.h"
#include "pixie-raw.h"

#define u8 uint8_t
static inline pix1 pix1_of_bytes (u8) __pure2;
static inline pix2 pix2_of_bytes (u8, u8) __pure2;
static inline pix4 pix4_of_bytes (u8, u8, u8, u8) __pure2;
static inline pix8 pix8_of_bytes (u8, u8, u8, u8, u8, u8, u8, u8) __pure2;


static const unsigned gbpb_maskb = 0xFF;
static const unsigned gbpb_byte_offset_0 =  0U;
static const unsigned gbpb_byte_offset_1 =  8U;
static const unsigned gbpb_byte_offset_2 = 16U;
static const unsigned gbpb_byte_offset_3 = 24U;
static const unsigned gbpb_byte_offset_4 = 32U;
static const unsigned gbpb_byte_offset_5 = 40U;
static const unsigned gbpb_byte_offset_6 = 48U;
static const unsigned gbpb_byte_offset_7 = 56U;

#define byte_N_of_pixM(N,M) \
static inline u8 __pure2 byte_##N##_of_pix##M (pix##M it) \
{ \
	return ((pix##M)(it) >> gbpb_byte_offset_##N) & gbpb_maskb; \
}

byte_N_of_pixM(0, 1)

byte_N_of_pixM(0, 2)
byte_N_of_pixM(1, 2)

byte_N_of_pixM(0, 4)
byte_N_of_pixM(1, 4)
byte_N_of_pixM(2, 4)
byte_N_of_pixM(3, 4)

byte_N_of_pixM(0, 8)
byte_N_of_pixM(1, 8)
byte_N_of_pixM(2, 8)
byte_N_of_pixM(3, 8)
byte_N_of_pixM(4, 8)
byte_N_of_pixM(5, 8)
byte_N_of_pixM(6, 8)
byte_N_of_pixM(7, 8)

////////////////////////////////////////////////////////////////////////

#define pixM_of_byte_N(N,M) \
static inline pix##M __pure2 pix##M##_of_byte_##N (u8 x) \
{ \
	return ((pix##M)(x) & (gbpb_maskb)) << gbpb_byte_offset_##N;	\
}

pixM_of_byte_N(0, 1)

pixM_of_byte_N(0, 2)
pixM_of_byte_N(1, 2)

pixM_of_byte_N(0, 4)
pixM_of_byte_N(1, 4)
pixM_of_byte_N(2, 4)
pixM_of_byte_N(3, 4)

pixM_of_byte_N(0, 8)
pixM_of_byte_N(1, 8)
pixM_of_byte_N(2, 8)
pixM_of_byte_N(3, 8)
pixM_of_byte_N(4, 8)
pixM_of_byte_N(5, 8)
pixM_of_byte_N(6, 8)
pixM_of_byte_N(7, 8)

static inline pix1
pix1_of_bytes (u8 a)
{
	return pix1_of_byte_0 (a);
}

static inline pix2
pix2_of_bytes (u8 a, u8 b)
{
	return
		pix2_of_byte_0 (a) |
		pix2_of_byte_1 (b);
}

static inline pix4
pix4_of_bytes (u8 a, u8 b, u8 c, u8 d)
{
	return
		pix4_of_byte_0 (a) |
		pix4_of_byte_1 (b) |
		pix4_of_byte_2 (c) |
		pix4_of_byte_3 (d);
}

static inline pix8
pix8_of_bytes (u8 a, u8 b, u8 c, u8 d, u8 e, u8 f, u8 g, u8 h)
{
	return
		pix8_of_byte_0 (a) |
		pix8_of_byte_1 (b) |
		pix8_of_byte_2 (c) |
		pix8_of_byte_3 (d) |
		pix8_of_byte_4 (e) |
		pix8_of_byte_5 (f) |
		pix8_of_byte_6 (g) |
		pix8_of_byte_7 (h);
}
