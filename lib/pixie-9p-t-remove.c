/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-remove.c
 * @brief 9P implementation: 9P2000 Tremove message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_remove)
{
	return
		size (pixfid);
}

SER(pix_9p_t_remove)
{
	SER_PRE (size (pix_9p_t_remove, from));

	ser (pixfid, &from->fid, into);

	return (ptrdiff_t) size (pix_9p_t_remove, from);
}

DE(pix_9p_t_remove)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_remove out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pix4, &into->fid);

	return total;
}

FMT(pix_9p_t_remove)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_remove);
	CHECK (writef ("Tremove"));
	CHECK (writef (" tag ")); CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid ")); CHECK (fmt (pixfid, &self->fid));
	return 0;
}

EQ(pix_9p_t_remove)
{
	return
		eq (a, b, pixfid, fid);
}

////////////////////////////////////////////////////////////////////////
