/*-
 * SPDX-License-Identifier: BSD-2-Clause AND MIT
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 *
 * Portions derived from the Plan 9 from Bell Labs distribution,
 * which is MIT licensed by the Plan 9 Foundation.
 * Contains code from
 *   + //plan9/sys/src/libc/9sys/fcallfmt.c
 *   + //plan9/sys/include/fcall.h
 *   + //plan9/sys/include/libc.h
 */

#pragma once

/**
 * @file  pixie.h
 * @brief yet another 9P2000 client/server library, in C
 *
 * ... but with some really weird constraints.
 * For example, no I/O: users provide us buffers of bytes.
 * For example, it's callbacks all the way down.
 */

#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <wchar.h>


////////////////////////////////////////////////////////////////////////

/** @defgroup	types	Low-level types and macros */
/** @{ */

/** Bitmask to note versions of the 9P protocol. */
typedef enum pix_dialect {
	PIX_9P2000   = 1 << 1,
	PIX_9P2000_u = 1 << 2,
	PIX_9P2000_L = 1 << 3,
} pixV;

typedef uint8_t  pix1;
typedef uint16_t pix2;
typedef uint32_t pix4;
typedef uint64_t pix8;

typedef struct pix_u8str {
	pix2 s;
	pix1 *text;
} pixS;

/** Make a copy of `str` as a 9P-style UTF string in `s`. */
pixS *pixS_of_cstr  (pixS *s, const char *str);

/** Make a copy of `str` as a 9P-style UTF string in `s`. */
pixS *pixS_of_wcstr (pixS *s, const wchar_t *str);

typedef struct pix_qid {
	pix1 qtype;
	/* From `//plan9/sys/include/libc.h' --- */
#define PIX_QTDIR       0x80    /**< type bit for directories */
#define PIX_QTAPPEND    0x40    /**< type bit for append only files */
#define PIX_QTEXCL      0x20    /**< type bit for exclusive use files */
#define PIX_QTMOUNT     0x10    /**< type bit for mounted channel */
#define PIX_QTAUTH      0x08    /**< type bit for authentication file */
#define PIX_QTTMP       0x04    /**< type bit for not-backed-up file */
#define PIX_QTFILE      0x00    /**< plain file */
	pix4 qver;
	pix8 qpath;
} pixqid;

typedef struct pix_dent {
	pix2   size;
	pix2   type;
	pix4   dev;
	pixqid qid;
	pix4   mode;
	/* From `//plan9/sys/include/libc.h' --- */
#define PIX_DMDIR       0x80000000      /**< mode bit for directories */
#define PIX_DMAPPEND    0x40000000      /**< mode bit for append only files */
#define PIX_DMEXCL      0x20000000      /**< mode bit for exclusive use files */
#define PIX_DMMOUNT     0x10000000      /**< mode bit for mounted channel */
#define PIX_DMAUTH      0x08000000      /**< mode bit for authentication file */
#define PIX_DMTMP       0x04000000      /**< mode bit for non-backed-up files */
#define PIX_DMREAD      0x4             /**< mode bit for read permission */
#define PIX_DMWRITE     0x2             /**< mode bit for write permission */
#define PIX_DMEXEC      0x1             /**< mode bit for execute permission */
	pix4   atime;
	pix4   mtime;
	pix8   length;
	pixS   name;
	pixS   uid;
	pixS   gid;
	pixS   muid;
} pixdent;

#define PIX_MAXWELEM    16

/* From `//plan9/sys/include/libc.h' --- */
#define PIX_OREAD       0       /**< open for read */
#define PIX_OWRITE      1       /**< write */
#define PIX_ORDWR       2       /**< read and write */
#define PIX_OEXEC       3       /**< execute, == read but check execute permission */
#define PIX_OTRUNC      16      /**< or'ed in (except for exec), truncate file first */
#define PIX_OCEXEC      32      /**< or'ed in, close on exec */
#define PIX_ORCLOSE     64      /**< or'ed in, remove on close */
#define PIX_OEXCL       0x1000  /**< or'ed in, exclusive use (create only) */
// #define PIX_OBEHIND  0x2000  /**< use write behind for writes [for 9n] */

typedef pix4 pixfid;
#define NOFID (pixfid)~0U       /**< Dummy fid. */

typedef pix2 pixtag;
#define NOTAG (pixtag)(~0)      /**< Dummy tag. */

/** @} */


////////////////////////////////////////////////////////////////////////

struct pix_raw;                 /* see pix-raw.h */
struct pix_pcb;                 /* see pix-proto.h */
struct pix_tagman;              /* see pix-tagman.h */


////////////////////////////////////////////////////////////////////////
