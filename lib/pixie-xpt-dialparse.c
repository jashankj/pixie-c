/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 *
 * Portions derived from the plan9port distribution,
 * which is MIT licensed by Russ Cox.
 * Contains code from
 *   + //plan9port/src/lib9/_p9dialparse.c
 */

/**
 * @file	pixie-xpt-dialparse.c
 * @brief	parse 9P (and Plan 9 at large) dial strings
 */

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <bsd/sys/param.h>

#include "zf_log.h"

#include "pixie.h"
#include "pixie-xpt.h"

////////////////////////////////////////////////////////////////////////

static int p9dialparse_one (struct pix_dial *dial, char *netaddr);
static int p9dialparse_two (
	struct pix_dial *dial, char *network, char *netaddr);
static int p9dialparse_three (
	struct pix_dial *dial,
	char *network, char *netaddr, char *service);
static int p9dialparse_inner (
	struct pix_dial *ds,
	char *network, char *netaddr, char *service);
static int p9dial_net (const char *net);

static const char *p9networks[] = {
	[PIX_TCP]  = "tcp",
	[PIX_UDP]  = "udp",
	[PIX_UNIX] = "unix",
	[PIX_ANY]  = "net"
};


int
pix_dialparse (const char *dialstr, struct pix_dial *dial)
{
	if (dialstr == NULL || *dialstr == '\0') {
		ZF_LOGE ("bad dialstr");
		return -1;
	}

	if (dial == NULL) {
		ZF_LOGE ("bad dial");
		return -1;
	}

	char *addr, *bang1, *bang2;
	addr = strdup (dialstr);
	if ((bang1 = strchr (addr, '!')) == NULL)
		return p9dialparse_one (dial, addr);
	*bang1++ = '\0';
	if ((bang2 = strchr (bang1, '!')) == NULL)
		return p9dialparse_two (dial, addr, bang1);
	*bang2++ = '\0';
	return p9dialparse_three (dial, addr, bang1, bang2);
}

static int
p9dialparse_one (struct pix_dial *dial, char *netaddr)
{
	int ret = p9dialparse_inner (dial, NULL, netaddr, NULL);
	free (netaddr);
	return ret;
}

static int
p9dialparse_two (struct pix_dial *dial, char *network, char *netaddr)
{
	int ret = p9dialparse_inner (dial, network, netaddr, NULL);
	free (network);
	return ret;
}

static int
p9dialparse_three (
	struct pix_dial *dial,
	char *network, char *netaddr, char *service
) {
	int ret = p9dialparse_inner (dial, network, netaddr, service);
	free (network);
	return ret;
}

static int
p9dialparse_inner (
	struct pix_dial *dial,
	char *network, char *netaddr, char *service
) {
	assert (dial != NULL);
	assert (netaddr != NULL);

	int net;
	if (network != NULL && (net = p9dial_net (network)) == -1)
		dial->network = (enum pix_network) net;
	else
		dial->network = PIX_ANY;

	dial->netaddr = netaddr;
	dial->service = service;

	return 0;
}

static int
p9dial_net (const char *net)
{
	for (unsigned i = 0; i < nitems (p9networks); i++)
		if (strncmp (p9networks[i], net, 4) == 0)
			return (int) i;
	return -1;
}

////////////////////////////////////////////////////////////////////////
