/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-r-auth.c
 * @brief 9P implementation: 9P2000 Rauth message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_r_auth)
{
	return
		size (pixqid);
}

SER(pix_9p_r_auth)
{
	SER_PRE (size (pix_9p_r_auth, from));

	ser (pixqid, &from->aqid, into);

	return (ptrdiff_t) size (pix_9p_r_auth, from);
}

DE(pix_9p_r_auth)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_r_auth out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixqid, &into->aqid);

	return total;
}

FMT(pix_9p_r_auth)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, r_auth);
	CHECK (writef ("Rauth"));
	CHECK (writef (" tag "));  CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" aqid ")); CHECK (fmt (pixqid, &self->aqid));
	return 0;
}

EQ(pix_9p_r_auth)
{
	return
		eq (a, b, pixqid, aqid);
}

////////////////////////////////////////////////////////////////////////
