/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2022 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-client.c
 * @brief 9P client implementation
 *
 * Smells strongly of lib9pclient from plan9port.
 */

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <bsd/sys/cdefs.h>
#include <bsd/sys/param.h>
#include <bsd/sys/types.h>

#include <bsd/sys/queue.h>
#include <bsd/sys/sbuf.h>
#include <bsd/sys/tree.h>

#include <zf_log.h>
#include "buf.h"

#include "pixie.h"
#include "pixie-cb.h"
#include "pixie-client.h"
#include "pixie-proto.h"
#include "pixie-raw.h"
#include "pixie-tagman.h"
#include "pixie-xpt.h"

////////////////////////////////////////////////////////////////////////

#define zf(label, LEVEL, ...) \
	({ ZF_##LEVEL (__VA_ARGS__); goto label; })

////////////////////////////////////////////////////////////////////////

struct req;
struct pixc_file;
struct pixc_dir;
struct pix_client;

typedef CB_TYPE(pixc_auth_f) pixc_auth_cb;

struct req {
	RB_ENTRY(req)  tree;
	pixtag         tag;
	pixtype        type;
	struct pix_raw msg;
	union {
		pixc_auth_cb cb_auth;
	};
};

struct pixc_file {
	pixfid   fid;
	unsigned mode;
	off_t    off;
	pixqid   qid;
	pixc    *fs;
};

struct pixc_dir {
	struct pixc_file file;
	struct buf       dents;
};

struct pix_client {
	size_t             msize;
	pixfid             nextfid;
	struct pix_tagman *tagman;
	bool               pcb_owned;
	struct pix_pcb    *pcb;
	RB_HEAD(reqt, req) head;
};

////////////////////////////////////////////////////////////////////////

static inline int
reqcmp (struct req *e1, struct req *e2)
{
	assert (e1 != NULL);
	assert (e1->tag == e1->msg.tag);
	assert (e2 != NULL);
	assert (e2->tag == e2->msg.tag);

	return (int)(e1->tag) - (int)(e2->tag);
}

RB_PROTOTYPE(reqt, req, tree, reqcmp)
RB_GENERATE(reqt, req, tree, reqcmp)

////////////////////////////////////////////////////////////////////////

static struct pix_client *pix_client_new_inner (bool, struct pix_pcb *);

static int proto_trampoline (void *, struct pix_raw *);
static int pixc_trampoline (struct pix_client *, struct pix_raw *);
static struct req *req_by_tag (struct pix_client *self, pixtag that);
static void pixc_proto_tramp_fail (struct pix_raw *);

static struct req *pixc_request (pixc *, pixtype, pixtag);

static pixFid *pixFid_of_pixqid (pixc *cl, unsigned, pixfid, pixqid);

////////////////////////////////////////////////////////////////////////

struct pix_client *
pix_client_new (void)
{
	return pix_client_new_inner (true, pix_proto_new ());
}

struct pix_client *
pix_client_new_from_pcb (struct pix_pcb *pcb)
{
	return pix_client_new_inner (false, pcb);
}

static struct pix_client *
pix_client_new_inner (bool pcb_owned, struct pix_pcb *pcb)
{
	struct pix_client *new;
	if ((new = malloc (sizeof *new)) == NULL)
		zf (err0, LOGE, "malloc");
	new->pcb_owned = pcb_owned;
	if ((new->pcb = pcb) == NULL)
		zf (err1, LOGE, "invalid pcb passed");
	if ((new->tagman = pix_tagman_new ()) == NULL)
		zf (err2, LOGE, "pix_tagman_new");
	new->nextfid   = 1;
	new->msize     = BUFSIZ;
	RB_INIT(&new->head);
	return new;

err2:
	if (new->pcb_owned) pix_proto_drop (new->pcb);
err1:
	free (new);
err0:
	return NULL;
}

void
pix_client_drop (struct pix_client *self)
{
	if (self == NULL) return;
	if (self->pcb_owned) pix_proto_drop (self->pcb);
	pix_tagman_drop (self->tagman);
	free (self);
	return;
}

////////////////////////////////////////////////////////////////////////

static int
pix_Tauth (
	struct pix_raw *msg,
	const pixfid    afid,
	const char     *uname,
	const char     *aname)
{
	assert (msg->type == T_AUTH);
	msg->t_auth.afid  = afid;
	if (pixS_of_cstr (&msg->t_auth.uname, uname) == NULL)
		zf (err0, LOGE, "uname");
	if (pixS_of_cstr (&msg->t_auth.aname, aname) == NULL)
		zf (err1, LOGE, "aname");
	return 0;

err1:
	free (msg->t_auth.uname.text);
err0:
	return -1;
}

pixtag
pixc_auth_async (
	struct pix_client *self,
	const char        *uname,
	const char        *aname,
	pixc_auth_f       *cbfunc,
	void              *cbdata
) {
	assert (self != NULL);
	assert (cbfunc != NULL);

	pixtag tag;
	if ((tag = pix_tagman_alloc (self->tagman)) == NOTAG)
		zf (err0, LOGE, "no tags available");

	struct req *req;
	if ((req = pixc_request (self, T_AUTH, tag)) == NULL)
		zf (err1, LOGE, "creating 9P request");

	if (pix_Tauth (&req->msg, NOFID, uname, aname) != 0)
		zf (err2, LOGE, "creating Tauth request");

	req->cb_auth = CB_OF (pixc_auth_cb, cbfunc, cbdata);
	return 0;

err2:
	free (req);
err1:
	pix_tagman_free (self->tagman, tag);
err0:
	return -1;
}

static int
pixc_auth_async_continue (
	struct pix_client *self,
	struct req        *req,
	struct pix_raw    *msg
) {
	if (msg->type == R_ERROR) ZF_LOGF ("not yet");

	pixFid *file = pixFid_of_pixqid (self, 0, NOFID, msg->r_attach.qid);
	return CB_INVOKE (&req->cb_auth, self, file);
}

////////////////////////////////////////////////////////////////////////

static int
proto_trampoline (void *cbdata, struct pix_raw *msg)
{
	return pixc_trampoline (
		(struct pix_client *) cbdata,
		msg
	);
}

static int
pixc_trampoline (struct pix_client *self, struct pix_raw *msg)
{
	assert (self != NULL);
	assert (msg != NULL);

	struct req *req;
	if ((req = req_by_tag (self, msg->tag)) == NULL) {
		pixc_proto_tramp_fail (msg);
		return 0;
	}

	if (! pix_raw_reply_p (&req->msg, msg)) {
		pixc_proto_tramp_fail (msg);
		return 0;
	}

	switch (req->type) {
	case T_AUTH: return pixc_auth_async_continue (self, req, msg);

	default:
		__builtin_unreachable();
	}
}

static struct req *
req_by_tag (struct pix_client *self, pixtag that)
{
	struct req find = { .tag = that };
	return RB_FIND (reqt, &self->head, &find);
}

static void
pixc_proto_tramp_fail (struct pix_raw *msg)
{
	struct sbuf *buf = sbuf_new_auto ();
	if (pix_raw_fmt (msg, buf) != 0) ZF_LOGF ("aiiieee");
	sbuf_finish (buf);
	ZF_LOGW ("discarding message %s", sbuf_data (buf));
	sbuf_delete (buf);
}

////////////////////////////////////////////////////////////////////////

static struct req *
pixc_request (
	struct pix_client *self,
	pixtype            type,
	pixtag             tag
) {
	assert (self != NULL);

	struct req *new;
	if ((new = malloc (sizeof *new)) == NULL)
		zf (err0, LOGE, "malloc");
	new->type = new->msg.type = type;
	new->tag  = new->msg.tag  = tag;
	if (RB_INSERT (reqt, &self->head, new) != NULL)
		ZF_LOGF ("duplicate tag %d, impossible", tag);
	return new;

err0:
	free (new);
	return NULL;
}

static struct pixc_file *
pixFid_of_pixqid (
	struct pix_client *cl,
	unsigned           mode,
	pixfid             fid,
	pixqid             qid
) {
	assert (cl != NULL);

	struct pixc_file *new;
	if ((new = malloc (sizeof *new)) == NULL)
		zf (err0, LOGE, "pixFid %d: malloc failed", fid);
	new->fs   = cl;
	new->fid  = fid;
	new->qid  = qid;
	new->mode = mode;
	new->off  = 0;
	return new;

err0:
	free (new);
	return NULL;
}

////////////////////////////////////////////////////////////////////////
