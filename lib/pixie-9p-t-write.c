/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-t-write.c
 * @brief 9P implementation: 9P2000 Twrite message
 */

#include "pixie-raw.h"
#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

SIZE(pix_9p_t_write)
{
	return
		size (pixfid) +
		size (pix8) +
		size (pix4) +
		self->count;
}

SER(pix_9p_t_write)
{
	SER_PRE (size (pix_9p_t_write, from));

	ser (pixfid, &from->fid,    into);
	ser (pix8,   &from->offset, into);
	ser (pix4,   &from->count,  into);
	// TODO(jashankj): store data

	return (ptrdiff_t) size (pix_9p_t_write, from);
}

DE(pix_9p_t_write)
{
	assert (buf != NULL);

	ptrdiff_t total = 0;
	BUF data = { buf, len };

	struct pix_9p_t_write out;
	if (into == NULL) into = &out;
	DO_PARSE (&data, total, pixfid, &into->fid);
	DO_PARSE (&data, total, pix8,   &into->offset);
	DO_PARSE (&data, total, pix4,   &into->count);
	// TODO(jashankj): store data

	return total;
}

FMT(pix_9p_t_write)
{
	struct pix_raw *msg =
		__containerof (self, struct pix_raw, t_write);
	CHECK (writef ("Twrite"));
	CHECK (writef (" tag "));    CHECK (fmt (pixtag, &msg->tag));
	CHECK (writef (" fid "));    CHECK (fmt (pixfid, &self->fid));
	CHECK (writef (" offset ")); CHECK (fmt (pix8,   &self->offset));
	CHECK (writef (" count "));  CHECK (fmt (pix4,   &self->count));
	CHECK (writec (' '));
	return dumpsome (buf, self->count, self->data);
}

EQ(pix_9p_t_write)
{
	return
		eq (a, b, pixfid, fid) &&
		eq (a, b, pix8,   offset) &&
		eq (a, b, pix4,   count) &&
		memcmp (a->data, b->data, a->count);
}

////////////////////////////////////////////////////////////////////////
