/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

/**
 * @file  pixie-9p-pix4.c
 * @brief 9P implementation: 9P2000 primitive types
 */

#include "pixie-raw-msg.h"

////////////////////////////////////////////////////////////////////////

pixN_cmp(4)
/** Deserialize a `pix4'. */
pixN_de(4, (buf[0], buf[1], buf[2], buf[3]))
pixN_eq(4)
FMT0(pix4) { return writef ("%" PRIu32, *self); }

/** Serialise a `pix4'. */
SER0(pix4)
{
	SER_PRE (size (pix4));

	u8 x0 = byte_0_of_pix4 (*from);
	buf_append (into, &x0, 1);
	u8 x1 = byte_1_of_pix4 (*from);
	buf_append (into, &x1, 1);
	u8 x2 = byte_2_of_pix4 (*from);
	buf_append (into, &x2, 1);
	u8 x3 = byte_3_of_pix4 (*from);
	buf_append (into, &x3, 1);

	return (ptrdiff_t) size (pix4);
}

SIZE_(pix4) { return 4; }

////////////////////////////////////////////////////////////////////////
