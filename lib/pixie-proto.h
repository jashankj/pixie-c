/*-
 * SPDX-License-Identifier: BSD-2-Clause
 * Copyright (c) 2021-22 The University of New South Wales, Australia.
 */

#pragma once

/**
 * @file  pixie-proto.h
 * @brief 9P implementation: protocol
 */

#include "pixie.h"
#include "pixie-raw.h"

////////////////////////////////////////////////////////////////////////

/**
 * Protocol control block: tracks information about a 9P service, where
 * we are either the client or the server.
 *
 * The protocol codec is callback-driven: after initialising, a client
 * should register callbacks via `pix_proto_register_cb', which, when
 * the codec receives and decodes a message matching the registered
 * type, are dispatched to that callback with a decoded message.
 */
struct pix_pcb;

/**
 * The type of codec callbacks.
 */
typedef int pix_proto_cb (void *cbdata, struct pix_raw *msg);

/**
 * Initialise a 9P protocol codec.
 */
struct pix_pcb *pix_proto_new (void);

/**
 * Release all resources associated with a 9P protocol codec
 */
void pix_proto_drop (struct pix_pcb *self);

/**
 * Set the 9P protocol dialect that a codec will understand.  The bits
 * in `dialect_mask' should be OR'd `enum pix_dialect' variants.
 */
int pix_proto_set_dialect (struct pix_pcb *self,
	unsigned dialect_mask);

/**
 * Register a callback to run when a protocol codec receives a message
 * of that type.
 *
 * @param	type	callback to run on this type of message
 * @param	cbfunc	function to invoke
 * @param	cbdata	data to pass to the callback
 */
int pix_proto_register_cb (struct pix_pcb *self,
	enum pix_type type, pix_proto_cb *cbfunc, void *cbdata);

/**
 * Unregister an already-registered callback.
 * @param	type	callback to run on this type of message
 * @param	cbfunc	function to invoke
 * @param	cbdata	data to pass to the callback
 */
int pix_proto_unregister_cb (struct pix_pcb *self,
	enum pix_type type, pix_proto_cb *cbfunc, void *cbdata);

/**
 * Receive some data from some underlying transport.
 *
 * @param	inbuf	a buffer of bytes available to parse.
 * @param	inlen	number of bytes available in `inbuf'
 */
int pix_proto_recv_bytes (struct pix_pcb *self,
	const uint8_t *inbuf, const size_t inlen);

/**
 * Receive a whole message from some underlying transport.
 *
 * @param	msg	the parsed message (borrowed)
 */
int pix_proto_recv_msg (struct pix_pcb *self,
	struct pix_raw *msg);

////////////////////////////////////////////////////////////////////////
